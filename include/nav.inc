<div class="row linha" id="menu">
    <div class="col-md-12">
        <nav class="topnav" id="myTopnav">
            <a href="../controller/listaPaciente.controller.php"><i class="fa fa-user fa-lg"></i> Meus Pacientes</a>
            <a href="../controller/listaSom.controller.php"><i class="fa fa-music fa-lg"></i> Meus Sons</a>
            <a href="../controller/selecionarPacienteTeste.controller.php"><i class="fa fa-headphones fa-lg"></i> Realizar Teste</a>
            <a href="../controller/logoff.controller.php"><i class="fa fa-sign-out fa-lg"></i> Sair do Sistema</a>
            <a href="javascript:void(0);" class="icon" onclick="toggleMenuHamburger()">&#9776;</a>
        </nav>
    </div>
</div>