<!-- head tags -->
<title>Sistema DPAC</title>
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 

<!-- Font Awesome -->
<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">

<!-- DATEPICKER -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="../datepicker/datepicker-pt-BR.js"></script>


<!-- BOOTSTRAP SELECT -->
<link href="../bootstrap-select-master/css/bootstrap-select.css" rel="stylesheet">
<script src="../bootstrap-select-master/js/bootstrap-select.js"></script>

<!-- BOOTSTRAP SLIDER -->
<link rel="stylesheet" href="../bootstrap-slider-master/dist/css/bootstrap-slider.css" />
<script src="../bootstrap-slider-master/dist/bootstrap-slider.js"></script>

<!-- meu js -->
<script src="../functions/functions.js"></script>

<!-- meu CSS -->
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" type="text/css" href="../css/topMenu.css">


