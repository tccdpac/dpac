<!-- CADASTRO DE PACIENTE -->
<div class="container-fluid divCadastroPaciente">
    <form id="formCadastroPaciente" method="post" action="">
        <input type="hidden" name="idUsuario" value="<?php echo $_SESSION['idUsuario']?>">

        <!-- NOME -->
        <div class="row linha">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input class="form-control" type="text" name="nomePaciente" placeholder="Nome Completo" required>
                </div>
            </div>
        </div>

        <!-- DATA NASCIMENTO -->
        <div class="row linha">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input class="form-control datepicker" type="text" name="dataNascPaciente" placeholder="Data de Nascimento">
                </div>
            </div>
        </div>

        <div class="row linha">
            <!-- NOME DOC -->
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-id-card"></i></span>
                    <input class="form-control" type="text" name="nomeDocIdentificacaoPaciente" placeholder="Tipo de Documento">
                </div>
            </div>

            <!-- NUM DOC -->
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                    <input class="form-control" type="text" name="numDocIdentificacaoPaciente" placeholder="Número do documento">
                </div>
            </div>
        </div>
            
        <div class="row linha">
            <!-- NATURALIDADE -->
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                    <input class="form-control" type="text" name="naturalidadePaciente" placeholder="Naturaliade">
                </div>
            </div>

            <!-- NACIONALIDADE -->
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
                    <input class="form-control" type="text" name="nacionalidadePaciente" placeholder="Nacionalidade">
                </div>
            </div>
        </div>

        <!-- OBSERVACAO -->
        <div class="row linha">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                    <textarea class="form-control" type="text" name="observacaoPaciente" placeholder="Observação" rows="3"></textarea>
                </div>
            </div>
        </div>

        <!-- RESPONSÁVEL -->
        <div class="row linha">
            <div class="col-md-12">
                <button class="btn btn-success col-md-1" onclick="adicionaCampoResponsavel();">+</button>
            </div>            
        </div>

        <div class="row linha">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input class="form-control" type="text" name="nomeResponsavel" placeholder="Nome do Responsável">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input class="form-control" type="text" name="telefoneResponsavel" placeholder="Telefone do Responsável">
                </div>
            </div>
        </div>

        <!-- SUBMIT -->
        <div class="row linha">
            <div class="col-md-12">
                <button class="btn btn-success" style="width: 100%" type="submit">
                    <i class="fa fa-check"></i>
                    Cadastrar
                </button>
            </div>
        </div>
    </form>
    <!-- MENSAGENS DE STATUS -->
    <div class="row linha">
        <div class="col-md-12">
            <?php
            exibeResultadoOuErro();
            ?>     
        </div>
    </div>
</div>