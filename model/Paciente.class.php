<?php

class Paciente {
    private $idPaciente;
    private $idUsuario;
    private $nomePaciente;
    private $dataNascPaciente;
    private $nomeDocIdentificacaoPaciente;
    private $numDocIdentificacaoPaciente;
    private $naturalidadePaciente;
    private $nacionalidadePaciente;
    private $observacaoPaciente;

    public function setIdPaciente($idPaciente) {
        $this->idPaciente = $idPaciente;
    }
    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }
    public function setNomePaciente($nomePaciente) {
        $this->nomePaciente = $nomePaciente;
    }
    public function setDataNascPaciente($dataNascPaciente) {
        $this->dataNascPaciente = $dataNascPaciente;
    }
    public function setNomeDocIdentificacaoPaciente($nomeDocIdentificacaoPaciente) {
        $this->nomeDocIdentificacaoPaciente = $nomeDocIdentificacaoPaciente;
    }
    public function setNumDocIdentificacaoPaciente($numDocIdentificacaoPaciente) {
        $this->numDocIdentificacaoPaciente = $numDocIdentificacaoPaciente;
    }
    public function setNaturalidadePaciente($naturalidadePaciente) {
        $this->naturalidadePaciente = $naturalidadePaciente;
    }
    public function setNacionalidadePaciente($nacionalidadePaciente) {
        $this->nacionalidadePaciente = $nacionalidadePaciente;
    }
    public function setObservacaoPaciente($observacaoPaciente) {
        $this->observacaoPaciente = $observacaoPaciente;
    }

    public function getIdPaciente() {
        return $this->idPaciente;
    }
    public function getIdUsuario() {
        return $this->idUsuario;
    }
    public function getNomePaciente() {
        return $this->nomePaciente;
    }
    public function getDataNascPaciente() {
        return $this->dataNascPaciente;
    }
    public function getNomeDocIdentificacaoPaciente() {
        return $this->nomeDocIdentificacaoPaciente;
    }
    public function getNumDocIdentificacaoPaciente() {
        return $this->numDocIdentificacaoPaciente;
    }
    public function getNaturalidadePaciente() {
        return $this->naturalidadePaciente;
    }
    public function getNacionalidadePaciente() {
        return $this->nacionalidadePaciente;
    }
    public function getObservacaoPaciente() {
        return $this->observacaoPaciente;
    }
}