<?php

class Usuario 
{
    private $idUsuario;
    private $nomeUsuario;
    private $emailUsuario;
    private $senhaUsuario;

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }
    public function setNomeUsuario($nomeUsuario) {
        $this->nomeUsuario = $nomeUsuario;
    }
    public function setEmailUsuario($emailUsuario) {
        $this->emailUsuario = $emailUsuario;
    }
    public function setSenhaUsuario($senhaUsuario) {
        $this->senhaUsuario = $senhaUsuario;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }
    public function getNomeUsuario() {
        return $this->nomeUsuario;
    }
    public function getEmailUsuario() {
        return $this->emailUsuario;
    }
    public function getSenhaUsuario() {
        return $this->senhaUsuario;
    }
}