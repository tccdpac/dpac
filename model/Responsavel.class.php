<?php

class Responsavel {
    
    private $idResponsavel;
    private $idPaciente;
    private $nomeResponsavel;
    private $numeroTelefoneResponsavel;

    public function getIdResponsavel () {
        return $this->idResponsavel;
    }
    public function getIdPaciente () {
        return $this->idPaciente;
    }
    public function getNomeResponsavel () {
        return $this->nomeResponsavel;
    }
    public function getNumeroTelefoneResponsavel () {
        return $this->numeroTelefoneResponsavel;
    }

    public function setIdResponsavel ($idResponsavel) {
        $this->idResponsavel = $idResponsavel;
    }
    public function setIdPaciente ($idPaciente) {
        $this->idPaciente = $idPaciente;
    }
    public function setNomeResponsavel ($nomeResponsavel) {
        $this->nomeResponsavel = $nomeResponsavel;
    }
    public function setNumeroTelefoneResponsavel ($numeroTelefoneResponsavel) {
        $this->numeroTelefoneResponsavel = $numeroTelefoneResponsavel;
    }


}