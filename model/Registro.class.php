<?php

class Registro {
    private $idRegistro;
    private $idPaciente;
    private $descricaoRegistro;
    private $dataRegistro;

    public function setIdRegistro($idRegistro){
        $this->idRegistro = $idRegistro;
    }
    public function setIdPaciente($idPaciente){
        $this->idPaciente = $idPaciente;
    }
    public function setDescricaoRegistro($descricaoRegistro){
        $this->descricaoRegistro = $descricaoRegistro;
    }
    public function setDataRegistro($dataRegistro){
        $this->dataRegistro = $dataRegistro;
    }
    public function getIdRegistro(){
        return $this->idRegistro;
    }
    public function getIdPaciente(){
        return $this->idPaciente;
    }
    public function getDescricaoRegistro(){
        return $this->descricaoRegistro;
    }
    public function getDataRegistro(){
        return $this->dataRegistro;
    }
}