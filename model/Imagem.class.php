<?php
class Imagem {
    private $idImagem;
    private $idSom;
    private $idUsuario;
    private $caminhoArquivoImagem;
    private $nomeImagem;

    public function getIdImagem (){
        return $this->idImagem;
    }
    public function getIdSom (){
        return $this->idSom;
    }
    public function getIdUsuario () {
        return $this->idUsuario;
    }
    public function getCaminhoArquivoImagem () {
        return $this->caminhoArquivoImagem;
    }
    public function getNomeImagem () {
        return $this->nomeImagem;
    }

    public function setIdImagem ($idImagem){
        $this->idImagem = $idImagem;
    }
    public function setIdSom ($idSom){
        $this->idSom = $idSom;
    }
    public function setIdUsuario ($idUsuario){
        $this->idUsuario = $idUsuario;
    }
    public function setCaminhoArquivoImagem ($caminhoArquivoImagem) {
        $this->caminhoArquivoImagem = $caminhoArquivoImagem;
    }
    public function setNomeImagem($nomeImagem) {
        $this->nomeImagem = $nomeImagem;
    }
}