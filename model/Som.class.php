<?php

class Som {
    private $idSom;
    private $idUsuario;
    private $caminhoArquivoSom;
    private $nomeSom;
    private $tipoSom;
    private $imagem;

    public function converteTipoSom($tipoSom){
        switch ($tipoSom) {
            case 1: {
                return 'ruido';
                break;
            }
            case 2: {
                return 'principal';
                break;
            }
            case 'ruido': {
                return 1;
                break;
            }
            case 'principal': {
                return 2;
                break;
            }
            default:
                return false;
                break;
        }
    }

    public function getIdSom (){
        return $this->idSom;
    }
    public function getIdUsuario () {
        return $this->idUsuario;
    }
    public function getCaminhoArquivoSom () {
        return $this->caminhoArquivoSom;
    }
    public function getNomeSom () {
        return $this->nomeSom;
    }
    public function getTipoSom (){
        return $this->tipoSom;
    }
    public function getImagem(){
        return $this->imagem;
    }

    public function setIdSom ($idSom){
        $this->idSom = $idSom;
    }
    public function setIdUsuario ($idUsuario){
        $this->idUsuario = $idUsuario;
    }
    public function setCaminhoArquivoSom ($caminhoArquivoSom) {
        $this->caminhoArquivoSom = $caminhoArquivoSom;
    }
    public function setNomeSom($nomeSom) {
        $this->nomeSom = $nomeSom;
    }
    public function setTipoSom($tipoSom) {
        $this->tipoSom = $tipoSom;
    }
    public function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    
}