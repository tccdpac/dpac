<?php
require_once "ConexaoDB.class.php";
require_once "../model/Responsavel.class.php";

class ResponsavelDao {

    public static function inserirResponsavel ($responsavel) {
        $pdo = ConexaoDB::conectar();

        $idPaciente = $responsavel->getIdPaciente();
        $nomeResponsavel = $responsavel->getNomeResponsavel();
        $numeroTelefoneResponsavel = $responsavel->getNumeroTelefoneResponsavel();

        $statement = $pdo->prepare(
                'insert into Responsaveis'
                . '(idPaciente, nomeResponsavel, numeroTelefoneResponsavel)'
                . ' values(:idPaciente, :nomeResponsavel, :numeroTelefoneResponsavel);');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);
        $statement->bindParam(':nomeResponsavel', $nomeResponsavel, PDO::PARAM_STR);
        $statement->bindParam(':numeroTelefoneResponsavel', $numeroTelefoneResponsavel, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $pdo = null;
            $statement = null;
            return $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $pdo = null;
            $statement = null;
            return $arrayErroPDO;
        }
    }

    public static function atualizarResponsavel($responsavel) {
        $pdo = ConexaoDB::conectar();
        
        $idPaciente = $responsavel->getIdPaciente();
        $idResponsavel = $responsavel->getIdResponsavel();
        $nomeResponsavel = $responsavel->getNomeResponsavel();
        $numeroTelefoneResponsavel = $responsavel->getNumeroTelefoneResponsavel();

        $statement = $pdo->prepare('
            update Responsaveis 
            SET 
            nomeResponsavel = :nomeResponsavel, 
            numeroTelefoneResponsavel = :numeroTelefoneResponsavel 
            where 
            idPaciente = :idPaciente and 
            idResponsavel = :idResponsavel
        ');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);
        $statement->bindParam(':idResponsavel', $idResponsavel, PDO::PARAM_INT);
        $statement->bindParam(':nomeResponsavel', $nomeResponsavel, PDO::PARAM_STR);
        $statement->bindParam(':numeroTelefoneResponsavel', $numeroTelefoneResponsavel, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $pdo = null;
            $statement = null;
            return $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $pdo = null;
            $statement = null;
            return $arrayErroPDO;
        }
    }

    public static function removerResponsavel ($idResponsavel) {
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare('delete from Responsaveis where idResponsavel = :idResponsavel');

        $statement->bindParam(':idResponsavel', $idResponsavel, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirResponsavelPaciente ($idPaciente) {
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare('delete from Responsaveis where idPaciente = :idPaciente');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscarResponsaveisPorPaciente ($idPaciente) {
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('select * from Responsaveis where idPaciente = :idPaciente');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Responsavel");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }

        $pdo = null;
        $statement = null;
        return $retorno;
    }


}