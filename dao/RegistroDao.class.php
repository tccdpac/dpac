<?php
require_once "ConexaoDB.class.php";
require_once "../model/Registro.class.php";

class RegistroDao {
    
    public static function inserirRegistro ($registro){
        $pdo = ConexaoDB::conectar();
        
        $idPaciente = $registro->getIdPaciente();
        $descricaoRegistro = $registro->getDescricaoRegistro();
        $dataRegistro = $registro->getDataRegistro();
        
        $statement = $pdo->prepare(
                'insert into Registros'
                . '(idPaciente, descricaoRegistro, dataRegistro)'
                . ' values(:idPaciente, :descricaoRegistro, :dataRegistro);');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);
        $statement->bindParam(':descricaoRegistro', $descricaoRegistro, PDO::PARAM_STR);
        $statement->bindParam(':dataRegistro', $dataRegistro, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function atualizarRegistro ($registro){
        $pdo = ConexaoDB::conectar();
        
        $idRegistro = $registro->getIdRegistro();
        $descricaoRegistro = $registro->getDescricaoRegistro();
        $dataRegistro = $registro->getDataRegistro();
        
        $statement = $pdo->prepare(
                'update Registros 
                SET
                descricaoRegistro = :descricaoRegistro, 
                dataRegistro = :dataRegistro
                WHERE 
                idRegistro = :idRegistro ');

        $statement->bindParam(':idRegistro', $idRegistro, PDO::PARAM_INT);
        $statement->bindParam(':descricaoRegistro', $descricaoRegistro, PDO::PARAM_STR);
        $statement->bindParam(':dataRegistro', $dataRegistro, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirRegistrosPaciente ($idPaciente){
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('delete from Registros WHERE idPaciente = :idPaciente');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirUmRegistroPaciente ($idRegistro) {
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('delete from Registros WHERE idRegistro = :idRegistro');

        $statement->bindParam(':idRegistro', $idRegistro, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscarRegistrosDoPaciente ($idPaciente) {
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('SELECT * FROM Registros WHERE idPaciente = :idPaciente ORDER BY dataRegistro DESC');

        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Registro");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }

        $pdo = null;
        $statement = null;
        return $retorno;
    }
}