<?php
require_once "ConexaoDB.class.php";
require_once "../model/Som.class.php";

class SomDao {

    public static function insereSom ($som){
        $pdo = ConexaoDB::conectar();
        
        $idUsuario = $som->getIdUsuario();
        $caminhoArquivoSom = $som->getCaminhoArquivoSom();
        $nomeSom = $som->getNomeSom();
        $tipoSom = $som->getTipoSom();

        $statement = $pdo->prepare(
                'insert into Sons'
                . '(idUsuario, caminhoArquivoSom, nomeSom, tipoSom)'
                . ' values(:idUsuario, :caminhoArquivoSom, :nomeSom, :tipoSom);');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':caminhoArquivoSom', $caminhoArquivoSom, PDO::PARAM_STR);
        $statement->bindParam(':nomeSom', $nomeSom, PDO::PARAM_STR);
        $statement->bindParam(':tipoSom', $tipoSom, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }        
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirSom ($idSom) {
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare(
            'DELETE from Sons
            WHERE idSom = :idSom');

        $statement->bindParam(':idSom', $idSom, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }        
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaSomPorId($idSom,$idUsuario) {
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare(
            'select * from Sons ' .
            'where idSom = :idSom and
            idUsuario = :idUsuario;'
        );

        $statement->bindParam(':idSom', $idSom, PDO::PARAM_INT);
        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Som");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaTodosOsSonsDoUsuario ($idUsuario) {
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare(
            'select * from Sons ' .
            'where idUsuario = :idUsuario;'
        );
        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Som");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaSonsPorTipo ($idUsuario, $tipoSom){
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare(
            'select * from Sons ' .
            'where idUsuario = :idUsuario '.
            'and tipoSom = :tipoSom;'
        );
        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':tipoSom', $tipoSom, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Som");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaSomPorIdUsuarioENomeSom($idUsuario, $nomeSom){
        $pdo = ConexaoDB::conectar();
        $statement = $pdo->prepare(
            'select * from Sons ' .
            'where idUsuario = :idUsuario and nomeSom = :nomeSom;'
        );
        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':nomeSom', $nomeSom, PDO::PARAM_STR);
        
        $resultado = $statement->execute();
        if($resultado){
            $statement->setFetchMode(PDO::FETCH_CLASS, 'Som');
            $fetch = $statement->fetch();
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }
}