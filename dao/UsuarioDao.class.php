<?php
require_once "ConexaoDB.class.php";
require_once "../model/Usuario.class.php";

class UsuarioDao {
    
    public static function inserirUsuario ($usuario){
        $pdo = ConexaoDB::conectar();

        $nomeUsuario = $usuario->getNomeUsuario();
        $emailUsuario = $usuario->getEmailUsuario();
        $senhaUsuario = $usuario->getSenhaUsuario();

        $statement = $pdo->prepare(
                'insert into Usuarios'
                . '(nomeUsuario, emailUsuario, senhaUsuario)'
                . ' values(:nomeUsuario, :emailUsuario, :senhaUsuario);');

        $statement->bindParam(':nomeUsuario', $nomeUsuario, PDO::PARAM_STR);
        $statement->bindParam(':emailUsuario', $emailUsuario, PDO::PARAM_STR);
        $statement->bindParam(':senhaUsuario', $senhaUsuario, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $pdo = null;
            $statement = null;
            return $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $pdo = null;
            $statement = null;
            return $arrayErroPDO;
        }
    }

    public static function buscaUsuarioPorEmailESenha ($usuario){
        $pdo = ConexaoDB::conectar();

        $emailUsuario = $usuario->getEmailUsuario();
        $senhaUsuario = $usuario->getSenhaUsuario();

        $statement = $pdo->prepare(
        'select idUsuario, nomeUsuario, emailUsuario, senhaUsuario from Usuarios where'
        . ' :emailUsuario = emailUsuario and'
        . ' :senhaUsuario = senhaUsuario;');

        $statement->bindParam(':emailUsuario', $emailUsuario, PDO::PARAM_STR);
        $statement->bindParam(':senhaUsuario', $senhaUsuario, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $statement->setFetchMode(PDO::FETCH_CLASS, 'Usuario');
            $fetch = $statement->fetch();
            $pdo = null;
            $statement = null;
            return $fetch;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $pdo = null;
            $statement = null;
            return $arrayErroPDO;
        }
        //todo separar o return
    }
}
