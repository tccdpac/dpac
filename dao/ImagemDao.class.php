<?php
require_once "ConexaoDB.class.php";
require_once "../model/Imagem.class.php";

class ImagemDao {

    public static function insereImagem ($imagem){
        $pdo = ConexaoDB::conectar();
        
        $idUsuario = $imagem->getIdUsuario();
        $idSom = $imagem->getIdSom();
        $caminhoArquivoImagem = $imagem->getCaminhoArquivoImagem();
        $nomeImagem = $imagem->getNomeImagem();

        $statement = $pdo->prepare(
                'insert into Imagens'
                . '(idUsuario, idSom, caminhoArquivoImagem, nomeImagem)'
                . ' values(:idUsuario, :idSom, :caminhoArquivoImagem, :nomeImagem);');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':idSom', $idSom, PDO::PARAM_INT);
        $statement->bindParam(':caminhoArquivoImagem', $caminhoArquivoImagem, PDO::PARAM_STR);
        $statement->bindParam(':nomeImagem', $nomeImagem, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }        
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirImagem ($idImagem) {
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare(
            'DELETE from Imagens
            WHERE idImagem = :idImagem');

        $statement->bindParam(':idImagem', $idImagem, PDO::PARAM_INT);
        
        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }        
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaImagemDoSom($idSom){
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare('select * from Imagens where idSom = :idSom');

        $statement->bindParam(':idSom', $idSom, PDO::PARAM_INT);

        $resultado = $statement->execute();
        
        if($resultado){
            $statement->setFetchMode(PDO::FETCH_CLASS, 'Imagem');
            $fetch = $statement->fetch();
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }
}