<?php
require_once "ConexaoDB.class.php";
require_once "../model/Paciente.class.php";

class PacienteDao {
    
    public static function inserirPaciente ($paciente){
        $pdo = ConexaoDB::conectar();

        $idUsuario = $paciente->getIdUsuario();
        $nomePaciente = $paciente->getNomePaciente();
        $dataNascPaciente = $paciente->getDataNascPaciente();
        
        $nomeDocIdentificacaoPaciente = $paciente->getNomeDocIdentificacaoPaciente();
        $numDocIdentificacaoPaciente = $paciente->getNumDocIdentificacaoPaciente();
        
        $naturalidadePaciente = $paciente->getNaturalidadePaciente();
        $nacionalidadePaciente = $paciente->getNacionalidadePaciente();
        $observacaoPaciente = $paciente->getObservacaoPaciente();

        $statement = $pdo->prepare(
                'insert into Pacientes'
                . '(idUsuario, nomePaciente, dataNascPaciente, 
                nomeDocIdentificacaoPaciente, numDocIdentificacaoPaciente, 
                naturalidadePaciente, nacionalidadePaciente, observacaoPaciente)'
                . ' values(:idUsuario, :nomePaciente, :dataNascPaciente, 
                :nomeDocIdentificacaoPaciente, :numDocIdentificacaoPaciente, 
                :naturalidadePaciente, :nacionalidadePaciente, :observacaoPaciente);');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':nomePaciente', $nomePaciente, PDO::PARAM_STR);
        $statement->bindParam(':dataNascPaciente', $dataNascPaciente, PDO::PARAM_STR);

        $statement->bindParam(':nomeDocIdentificacaoPaciente', $nomeDocIdentificacaoPaciente, PDO::PARAM_STR);
        $statement->bindParam(':numDocIdentificacaoPaciente', $numDocIdentificacaoPaciente, PDO::PARAM_STR);
        
        $statement->bindParam(':naturalidadePaciente', $naturalidadePaciente, PDO::PARAM_STR);
        $statement->bindParam(':nacionalidadePaciente', $nacionalidadePaciente, PDO::PARAM_STR);
        $statement->bindParam(':observacaoPaciente', $observacaoPaciente, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function atualizarPaciente ($paciente) {
        $pdo = ConexaoDB::conectar();
        
        $idUsuario = $paciente->getIdUsuario();
        $idPaciente = $paciente->getIdPaciente();
        $nomePaciente = $paciente->getNomePaciente();
        $dataNascPaciente = $paciente->getDataNascPaciente();
        
        $nomeDocIdentificacaoPaciente = $paciente->getNomeDocIdentificacaoPaciente();
        $numDocIdentificacaoPaciente = $paciente->getNumDocIdentificacaoPaciente();
        
        $naturalidadePaciente = $paciente->getNaturalidadePaciente();
        $nacionalidadePaciente = $paciente->getNacionalidadePaciente();
        $observacaoPaciente = $paciente->getObservacaoPaciente();

        $statement = $pdo->prepare('
            update Pacientes 
            SET 
            nomePaciente = :nomePaciente, 
            dataNascPaciente = :dataNascPaciente, 
            nomeDocIdentificacaoPaciente = :nomeDocIdentificacaoPaciente, 
            numDocIdentificacaoPaciente = :numDocIdentificacaoPaciente, 
            naturalidadePaciente = :naturalidadePaciente, 
            nacionalidadePaciente = :nacionalidadePaciente, 
            observacaoPaciente = :observacaoPaciente 
            where 
            idUsuario = :idUsuario and 
            idPaciente = :idPaciente
            ');
        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);
        $statement->bindParam(':nomePaciente', $nomePaciente, PDO::PARAM_STR);
        $statement->bindParam(':dataNascPaciente', $dataNascPaciente, PDO::PARAM_STR);

        $statement->bindParam(':nomeDocIdentificacaoPaciente', $nomeDocIdentificacaoPaciente, PDO::PARAM_STR);
        $statement->bindParam(':numDocIdentificacaoPaciente', $numDocIdentificacaoPaciente, PDO::PARAM_STR);
        
        $statement->bindParam(':naturalidadePaciente', $naturalidadePaciente, PDO::PARAM_STR);
        $statement->bindParam(':nacionalidadePaciente', $nacionalidadePaciente, PDO::PARAM_STR);
        $statement->bindParam(':observacaoPaciente', $observacaoPaciente, PDO::PARAM_STR);

        $resultado = $statement->execute();
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function excluirPacienteUsuario ($idPaciente, $idUsuario){
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare('delete from Pacientes where idUsuario = :idUsuario and idPaciente = :idPaciente');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);

        $resultado = $statement->execute();
        
        if($resultado){
            $retorno = $resultado;
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = $arrayErroPDO;
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaTodosOsPacientesDoUsuario($idUsuario){
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('select * from Pacientes where idUsuario = :idUsuario');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);

        $resultado = $statement->execute();
        if($resultado){
            $fetch = $statement->fetchAll(PDO::FETCH_CLASS, "Paciente");
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }

        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaPacienteUsuario($idPaciente, $idUsuario){
        $pdo = ConexaoDB::conectar();
        
        $statement = $pdo->prepare('select * from Pacientes where idUsuario = :idUsuario and idPaciente = :idPaciente');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':idPaciente', $idPaciente, PDO::PARAM_INT);

        $resultado = $statement->execute();
        
        if($resultado){
            $statement->setFetchMode(PDO::FETCH_CLASS, 'Paciente');
            $fetch = $statement->fetch();
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }
        
        $pdo = null;
        $statement = null;
        return $retorno;
    }

    public static function buscaPacientePorNome ($nomePaciente, $idUsuario){
        $pdo = ConexaoDB::conectar();

        $statement = $pdo->prepare('select * from Pacientes where idUsuario = :idUsuario and nomePaciente = :nomePaciente');

        $statement->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $statement->bindParam(':nomePaciente', $nomePaciente, PDO::PARAM_STR);

        $resultado = $statement->execute();
        
        if($resultado){
            $statement->setFetchMode(PDO::FETCH_CLASS, 'Paciente');
            $fetch = $statement->fetch();
            $retorno = array(true, $fetch);
        } else {
            $arrayErroPDO = $statement->errorInfo();
            $retorno = array(false, $arrayErroPDO);
        }

        $pdo = null;
        $statement = null;
        return $retorno;
    }
}