<?php
class ConexaoDB {
    public static function conectar () {
        
        $host = "localhost";
        $dbname = "dpac";
        $charset = "utf8mb4";
        $user = 'root';
        $pass = '';
        
        $pdo = new PDO(
            "mysql:host=$host;"
            . "dbname=$dbname;"
            . "charset=$charset",
            "$user",
            "$pass");
		
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        return $pdo;
    }
}
