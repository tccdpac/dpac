CREATE DATABASE dpac;
-- USE dpac;

CREATE TABLE Usuarios (
	idUsuario bigint not null AUTO_INCREMENT,
	nomeUsuario varchar(255) not null unique,
	emailUsuario varchar(255) not null unique,
	senhaUsuario varchar(255) not null,
	CONSTRAINT PK_idUsuario PRIMARY KEY (idUsuario)
);

CREATE TABLE Pacientes (
	idPaciente bigint not null AUTO_INCREMENT,
	idUsuario bigint not null,
    nomePaciente varchar(255) not null unique,
    dataNascPaciente varchar(255),
    nomeDocIdentificacaoPaciente varchar(255),
    numDocIdentificacaoPaciente varchar(255),
    naturalidadePaciente varchar(255),
    nacionalidadePaciente varchar(255),
    observacaoPaciente varchar(2000),
	CONSTRAINT PK_idPaciente PRIMARY KEY (idPaciente),
	CONSTRAINT FK_idUsuario FOREIGN KEY (idUsuario) REFERENCES Usuarios(idUsuario)
);

CREATE TABLE Responsaveis (
	idResponsavel bigint not null AUTO_INCREMENT,
	idPaciente bigint not null,
	nomeResponsavel varchar(255) not null,
	numeroTelefoneResponsavel varchar(20) not null,
	CONSTRAINT PK_idResponsavel PRIMARY KEY (idResponsavel),
	CONSTRAINT FK_idPaciente FOREIGN KEY (idPaciente) REFERENCES Pacientes(idPaciente)
);

CREATE TABLE Sons (
	idSom bigint not null AUTO_INCREMENT,
	idUsuario bigint not null,
	caminhoArquivoSom varchar(255) not null,
	nomeSom varchar(255) not null unique,
	tipoSom bigint not null,
	CONSTRAINT PK_idSom PRIMARY KEY (idSom),
	CONSTRAINT FK_idUsuario FOREIGN KEY (idUsuario) REFERENCES Usuarios(idUsuario)
);

CREATE TABLE Imagens (
	idImagem bigint not null AUTO_INCREMENT,
	idSom bigint not null,
	idUsuario bigint not null,
	caminhoArquivoImagem varchar(255) not null,
	nomeImagem varchar(255) not null unique,
	CONSTRAINT PK_idImagem PRIMARY KEY (idImagem),
	CONSTRAINT FK_idUsuario FOREIGN KEY (idUsuario) REFERENCES Usuarios(idUsuario),
	CONSTRAINT FK_idSom FOREIGN KEY (idSom) REFERENCES Sons(idSom)
);

CREATE TABLE Registros (
	idRegistro bigint not null AUTO_INCREMENT,
	idPaciente bigint not null,
	descricaoRegistro varchar(2000) not null,
    dataRegistro varchar(255) not null,
	CONSTRAINT PK_idRegistro PRIMARY KEY (idRegistro),
	CONSTRAINT FK_idPaciente FOREIGN KEY (idPaciente) REFERENCES Pacientes(idPaciente)
);