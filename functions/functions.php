<?php

function validaData($data){
    $data = explode("/", $data);

    if(count($data) != 3){
        return array(false, 13);
    }

    $anoAtual = date("Y");
    //checa ano
    if($data[2] > $anoAtual+1){
        return array(false, 14);
    }
    if($data[2] < $anoAtual-100){
        return array(false, 15);
    }

    //checa mês
    if( !checkdate($data[1], 01, 1990) ){
        return array(false, 16);
    }

    //checa dia
    if( !checkdate($data[1], $data[0], 1990) ){
        return array(false, 17);
    }

    return array(true, "Data válida!");

}

function phpRedireciona($url, $statusCode = 301)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

function redirecionaUsuarioAutenticado () {
    if(isset($_SESSION['idUsuario']) &&
        isset($_SESSION['nomeUsuario'])){
        phpRedireciona("../view/main.view.php");
    }
}

function redirecionaUsuarioNaoAutenticado () {
    if(!isset($_SESSION['idUsuario']) ||
        !isset($_SESSION['nomeUsuario'])){
        phpRedireciona("../view/login.view.php");
    }
}

function exibeResultadoOuErro ($numeracaoLocal = null) {
    if($numeracaoLocal == null){
        $numeracaoLocal = '';
    }
    if(isset($_GET["erro$numeracaoLocal"])){
        $erro = converteCodigoParaMensagem($_GET["erro$numeracaoLocal"]);
    }
    if(isset($_GET["resultado$numeracaoLocal"])){
        $resultado = converteCodigoParaMensagem($_GET["resultado$numeracaoLocal"]);
    }
    
    
    if(isset($resultado)){
        echo "
        <div class='alert alert-success'>
            <strong>Successo!</strong> $resultado
        </div>
        ";
    } elseif(isset($erro)) {
        echo "
        <div class='alert alert-danger'>
            <strong>Erro!</strong> $erro
        </div>
        ";
    }
}

function dataHoje () {
    return date("d/m/Y");
}

function converteCodigoParaMensagem ($codigo) {
    switch ($codigo) {
        case '1': {
            return "Campo 'nome' vazio.";
            break;
        }
        case '2': {
            return "Campo 'e-mail' vazio.";
            break;
        }
        case '3': {
            return "Campo 'senha' vazio.";
            break;
        }
        case '4': {
            return "Campo 'repita sua senha' está vazio.";
            break;
        }
        case '5': {
            return "As senhas são diferentes!";
            break;
        }
        case '6': {
            return "Cadastrado com sucesso!";
            break;
        }
        case '7': {
            return "Usuário não encontrado!";
            break;
        }
        case '8': {
            return "Paciente removido com sucesso!";
            break;
        }
        case '9': {
            return "Registro removido com sucesso!";
            break;
        }
        case '10': {
            return "Registro alterado com sucesso!";
            break;
        }
        case '11': {
            return "Som excluído com sucesso!";
            break;
        }
        case '12': {
            return "Som e imagem excluídos com sucesso!";
            break;
        }

        case '13':{
            return "Formato da data inválido!";
            break;
        }

        case '14':{
            return "Ano maior do que permitido!";
            break;
        }

        case '15':{
            return "Ano menor do que permitido!";
            break;
        }

        case '16':{
            return "Mês incorreto!";
            break;
        }

        case '17':{
            return "Dia incorreto!";
            break;
        }

        

        default: {
            return "Ocorreu um erro. Tente novamente em alguns instantes!<!-- DETALHES DO ERRO: ".$codigo." -->";
            break;
        }
           
    }
}