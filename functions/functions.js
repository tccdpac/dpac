/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function toggleMenuHamburger() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function ativaDatePicker() {
    
    //jQuery para que todo item com classe datepicker seja um datepicker
    //configurado para dia/mes/ano e para que, cada vez que uma data seja selecionada
    //o value do input seja atualizado com o val do datepicker.            
    $( ".datepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        onSelect: function(){
            var value = $(this).val()
            $(this).attr('value', value);
        }
    });
    
    //segunda parte para popular o datepicker.val com o input value
    $(".datepicker").each(function() {
        $(this).datepicker('setDate', $(this).val());
    });

}