<?php
require_once "../model/Paciente.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
            a {
                color: black;
            }
            .table > tbody > tr > td {
                vertical-align: middle;
            }
            
        </style>  
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>
            
            <!-- LISTA DE PACIENTES -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    if(isset($_SESSION["listaPacientes"])){
                        $listaPacientes = $_SESSION["listaPacientes"];
                        unset($_SESSION["listaPacientes"]);

                        echo "
                        <table class='table marginCentering'>
                            <tr>
                                <th>#Pac.</th>
                                <th>Nome Completo</th>
                                <th></th>
                            </tr>
                        ";

                        foreach ($listaPacientes as $paciente) {
                            $idPaciente = $paciente->getIdPaciente();
                            $nomePaciente = $paciente->getNomePaciente();
                            echo "
                            <tr>
                                <td>$idPaciente</td>
                                <td>$nomePaciente</td>
                                <td>
                                    <a href='../controller/realizarTeste.controller.php?paciente=$idPaciente'>
                                        <button class='btn btn-default'>
                                        Escolher <i class='fa fa-arrow-right' aria-hidden='true'></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            ";
                        }
                        echo "</table>";
                    } else {
                        echo "Você ainda não tem Pacientes cadastrados!";
                    }
                    ?>
                </div>
            </div>

            <!-- MENSAGENS DE STATUS -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    exibeResultadoOuErro();
                    ?>     
                </div>
            </div>
            <br><br>

        </div>
    </body>
</html>