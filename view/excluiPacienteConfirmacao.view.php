<?php
require_once "../model/Paciente.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

if(isset($_GET["idPaciente"])){
    $idPaciente = $_GET["idPaciente"];
} else {
    echo "Não Permitido, sem paciente!";
    exit();
}
if(isset($_GET["origem"])){
    $origem = $_GET["origem"];
} else {
    echo "Não Permitido, sem origem!";
    exit();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
        </style>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>

            <?php
            echo $origem;
            ?>
        </div>
    </body>
</html>