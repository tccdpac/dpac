<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
            input[type='file'] {
                display: none
            }
            
        </style>  
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>
        
        <div class="container-fluid">
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>
            
            <!-- O tipo de encoding de dados, enctype, DEVE ser especificado abaixo -->
            <form enctype="multipart/form-data" action="../controller/uploadDeSom.controller.php" method="POST">
            <input type="hidden" name="MAX_FILE_SIZE" value="1000000000000">
            
            <!-- DIV CENTRALIZADA -->
            <div class="container-fluid divCadastroPaciente">
                
                <!-- BOTÃO VOLTAR -->
                <div class="row linha">
                    <div class="col-md-12">
                        <a href="../controller/listaPaciente.controller.php" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a> 
                    </div>
                </div>
                <br>

                <!-- INPUT SOM -->
                <div class="row linha">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="btn btn-default" id="labelSom" for='inputSom'>
                            Escolha o arquivo 
                            <i class="fa fa-file-audio-o fa-lg" aria-hidden="true"></i>
                        </label>
                        <span id="nomeSom">Nenhum arquivo selecionado</span>
                        <input id="inputSom" name="inputSom" type="file" accept=".mp3">
                    </div>
                </div>

                <!-- RADIO BUTTONS -->
                <div class="row linha">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                        <div class="radio">
                            <label><input id="tipoDeSom1" type="radio" name="tipoDeSom" value="ruido" checked>Ruído</label>
                        </div>
                        <div class="radio">
                            <label><input id="tipoDeSom2" type="radio" name="tipoDeSom" value="principal">Principal</label>
                        </div>
                    </div>
                </div>

                <!-- INPUT IMAGEM -->
                <div class="row linha" id="rowImagem">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="btn btn-default" id="labelImagem" for='inputImagem'>
                            Escolha a imagem 
                            <i class="fa fa-file-image-o fa-lg" aria-hidden="true"></i>
                        </label>
                        <span id="nomeImagem">Nenhum arquivo selecionado</span>
                        <input id="inputImagem" name="inputImagem" type="file" accept=".jpg,.jpeg">
                    </div>
                </div>
                
                <br><br>
                <!-- SUBMIT -->
                <div class="row linha">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button class="btn btn-success" type="submit">
                            Enviar <i class="fa fa-upload fa-lg" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>

                <!-- MENSAGENS DE STATUS -->
                <div class="row linha">
                    <div class="col-md-12">
                        <?php
                        exibeResultadoOuErro();
                        ?>     
                    </div>
                </div>
            </div>
            </form>
        </div>
            
        <script>
            $(document).ready(function(){
                $('#rowImagem').hide();

                var inputSom = document.getElementById('inputSom');
                var nomeSom = document.getElementById('nomeSom');
                inputSom.addEventListener('change', function(){
                    nomeSom.textContent = inputSom.files[0].name
                });

                var inputImagem = document.getElementById('inputImagem');
                var nomeImagem = document.getElementById('nomeImagem');
                inputImagem.addEventListener('change', function(){
                    nomeImagem.textContent = inputImagem.files[0].name
                });

                var radio1 = document.getElementById('tipoDeSom1');
                radio1.addEventListener('click', function(){
                    $('#rowImagem').hide(300);
                });

                var radio2 = document.getElementById('tipoDeSom2');
                radio2.addEventListener('click', function(){
                    $('#rowImagem').show(300);
                });
            });
        </script>
    </body>
</html>