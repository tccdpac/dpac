<?php
require_once "../model/Som.class.php";
require_once "../model/Imagem.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
        img.thumbnail {
            width:50px;
        }
        </style>  
        <script>
            function toggleConfirmacao (duracao, numero) {
                $("#boxConfirmacao"+numero).toggle(duracao);
            }
        </script>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>
        
        <div class="container-fluid">
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>
            
            <!-- FUNÇÕES -->
            <div class="row linha textAlignCentering">
                <a class="btn btn-default" href="uploadDeSom.view.php">
                    Subir arquivo mp3
                    <i class='fa fa-upload fa-lg' aria-hidden='true'></i>
                </a>

                <a class="btn btn-default" href="gravarSom.view.php">
                    Gravar Som
                    <i class='fa fa-microphone fa-lg' aria-hidden='true'></i>
                </a>            
            </div>

            <!-- LISTA DE SONS -->
            <div class="row linha">
                <div class="col-md-12">
                <?php
                
                if( (isset($_SESSION["listaSons"])) && (!empty($_SESSION["listaSons"])) ){
                    $listaSons = $_SESSION["listaSons"];
                    unset($_SESSION["listaSons"]);

                    echo "
                    <table class='table marginCentering' style='max-width: 1500px;'>
                        <tr>
                            <th>#Som</th>
                            <th>Nome</th>
                            <th>Ouvir</th>
                            <th>Tipo</th>
                            <th>Funções</th>
                        </tr>
                    ";
                    foreach ($listaSons as $som) {
                        $idSom = $som->getIdSom();
                        $caminhoSom = $som->getCaminhoArquivoSom() . $som->getNomeSom();
                        $nomeSom = $som->getNomeSom();
                        $tipoSomHTML;
                        $imagem = $som->getImagem();                        
                        if( ($imagem != null) && ($som->getTipoSom() == 2) ){
                            $caminhoImagem = $imagem->getCaminhoArquivoImagem() . $imagem->getNomeImagem();
                            $tipoSomHTML = "    
                            Principal
                            <a href='../$caminhoImagem' target='blank'>
                            <img src='../$caminhoImagem' class='thumbnail'>
                            </a>";
                        } else {
                            $tipoSomHTML = "Ruído";
                        }
                        echo "
                        <tr>
                            <td>$idSom</td>
                            <td>$nomeSom</td>
                            <td>
                                <audio controls>
                                    <source src='../$caminhoSom' type='audio/mpeg'>
                                    Your browser does not support the audio element.
                                </audio>
                            </td>
                            <td>
                                $tipoSomHTML
                            </td>
                            <td style='width:40%;'>
                                <div class='btn-group'>
                                    <a href='#' onclick='toggleConfirmacao(400,$idSom)' aria-label='Excluir' class='btn btn-default'>
                                        <i class='fa fa-trash-o fa-2x' aria-hidden='true'></i>
                                    </a>
                                </div>
                                <span id='boxConfirmacao$idSom' style='display:none;'>
                                    Confirma a remoção do som (e imagem)? 
                                    <a class='btn btn-default' onclick='toggleConfirmacao(400,$idSom)'>Não</a> 
                                    <a class='btn btn-default' href='../controller/excluirSom.controller.php?idSom=$idSom'>Sim</a>
                                </span>
                            </td>
                        </tr>
                        ";
                    }
                    echo "</table>";
                } else {
                    echo "Você não tem Sons cadastrados!";
                }
                ?>
                </div>
            </div>
            <!-- MENSAGENS DE STATUS -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    exibeResultadoOuErro();
                    ?>     
                </div>
            </div>
            
        </div>
        <script>
            $(document).ready(function(){
                
            });
        </script>
    </body>
</html>