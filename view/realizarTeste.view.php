<?php
require_once "../model/Paciente.class.php";
require_once "../model/Som.class.php";
require_once "../model/Imagem.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();


$listaSons = $_SESSION["listaSons"];
$listaSonsRuido = [];
$listaSonsPrincipais = [];
foreach ($listaSons as $som) {
    if($som->getTipoSom() == 1){
        array_push($listaSonsRuido,$som);
    }
    if($som->getTipoSom() == 2){
        array_push($listaSonsPrincipais,$som);
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
            a {
                color: black;
            }
            .table > tbody > tr > td {
                vertical-align: middle;
            }
            .tv2 {
                width: 100%;
            }
            .moldura {
                width: 100%;
                z-index: -1;
            }
            .background {
                background-image: url("../");
                background-size: 100%;
                background-repeat: no-repeat;
                padding: 0px;
                background-position: center; 
            }
            #labelRuidoA, #labelRuidoB{
                font-size: 18px;
            }
            #labelPrincipal{
                font-size: 24px;
            }
            .orelha{
                width: 20px;
            }
            .mirror {
                -moz-transform: scale(-1, 1);
                -o-transform: scale(-1, 1);
                -webkit-transform: scale(-1, 1);
                transform: scale(-1, 1);
            }
            
        </style>
        <script>
        var listaSonsRuido = [];
        var listaSonsPrincipais = []
        // FUNCAO PARA BOTÃO MOSTRAR
        function mostrarEsconderControles (milisegundos) {
            $(".headerSistema").toggle(milisegundos);
            $("#menu").toggle(milisegundos);
        }
        $(document).ready(function() {
            mostrarEsconderControles(0);

            // ON CHANGE RUIDO A
            $("#selectRuidoA").on("change", function() {
                var idSom = $("#selectRuidoA").prop('value');
                for(var i = 0; i < listaSonsRuido.length ; i++){
                    var som = listaSonsRuido[i];
                    if(som.idSom == idSom){
                        $('#audioRuidoA source').prop('src','../'+som.caminhoArquivoSom+som.nomeSom);
                        $('#audioRuidoA').load();
                    }
                }
            });

            // ON CHANGE PRINCIPAL
            $("#selectPrincipal").on("change", function() {
                var idSom = $("#selectPrincipal").prop('value');
                for(var i = 0; i < listaSonsPrincipais.length ; i++){
                    var som = listaSonsPrincipais[i];
                    if(som.idSom == idSom){
                        $('#audioPrincipal source').prop('src','../'+som.caminhoArquivoSom+som.nomeSom);
                        $('#audioPrincipal').load();
                        var stringImagem = 'url("../'+ som.caminhoArquivoImagem + som.nomeImagem +'")';
                        
                        console.log(stringImagem);
                        $('.background').css('background-image',stringImagem);
                    }
                }
            });

            // ON CHANGE RUIDO B
            $("#selectRuidoB").on("change", function() {
                var idSom = $("#selectRuidoB").prop('value');
                for(var i = 0; i < listaSonsRuido.length ; i++){
                    var som = listaSonsRuido[i];
                    if(som.idSom == idSom){
                        $('#audioRuidoB source').prop('src','../'+som.caminhoArquivoSom+som.nomeSom);
                        $('#audioRuidoB').load();
                    }
                }
            });
             
            <?php
            // CAPTURA TODOS OS SONS RUIDO E CONVERTE PARA UM ARRAY DE OBJETOS JS
            foreach ($listaSonsRuido as $som) {
                $idSom = $som->getIdSom();
                $idUsuario = $som->getIdUsuario();
                $caminhoArquivoSom = $som->getCaminhoArquivoSom();
                $caminhoArquivoSom = explode("\\", $caminhoArquivoSom);
                $caminhoArquivoSom = '\\\\'.$caminhoArquivoSom[1].'\\\\'.$caminhoArquivoSom[2].'\\\\';
                $nomeSom = $som->getNomeSom();
                $tipoSom = $som->getTipoSom();
                echo "
                var som = {
                    idSom: $idSom,
                    idUsuario: $idUsuario,
                    caminhoArquivoSom: '$caminhoArquivoSom',
                    nomeSom: '$nomeSom',
                    tipoSom: $tipoSom
                };
                listaSonsRuido.push(som);
                ";
            }
            
            // CAPTURA TODOS OS SONS PRINCIPAIS E CONVERTE PARA UM ARRAY DE OBJETOS JS
            foreach ($listaSonsPrincipais as $som) {
                $idSom = $som->getIdSom();
                $idUsuario = $som->getIdUsuario();
                $caminhoArquivoSom = $som->getCaminhoArquivoSom();
                $caminhoArquivoSom = explode("\\", $caminhoArquivoSom);
                $caminhoArquivoSom = '\\\\'.$caminhoArquivoSom[1].'\\\\'.$caminhoArquivoSom[2].'\\\\';
                $nomeSom = $som->getNomeSom();
                $tipoSom = $som->getTipoSom();
                $imagem = $som->getImagem();
                $idImagem = $imagem->getIdImagem();
                $caminhoArquivoImagem = $imagem->getCaminhoArquivoImagem();

                $caminhoArquivoImagem = explode("\\", $caminhoArquivoImagem);
                $caminhoArquivoImagem = '/'.$caminhoArquivoImagem[1].'/'.$caminhoArquivoImagem[2].'/';

                $nomeImagem = $imagem->getNomeImagem();
                echo "
                var som = {
                    idSom: $idSom,
                    idUsuario: $idUsuario,
                    caminhoArquivoSom: '$caminhoArquivoSom',
                    nomeSom: '$nomeSom',
                    tipoSom: $tipoSom,
                    idImagem: $idImagem,
                    caminhoArquivoImagem: '$caminhoArquivoImagem',
                    nomeImagem: '$nomeImagem'
                };
                listaSonsPrincipais.push(som);
                ";
            }
            ?>

            // PREENCHER OS SELECTS DE RUÍDO COM OS OPTIONS
            listaSonsRuido.forEach(function(element) {
                $('#selectRuidoA').append(
                "<option value='"+
                element.idSom+
                "'>"+
                element.nomeSom+
                "</option>"
                );

                $('#selectRuidoB').append(
                "<option value='"+
                element.idSom+
                "'>"+
                element.nomeSom+
                "</option>"
                );
            }, this);

            // PREENCHER O SELECT DE SOM PRINCIPAL COM OS OPTIONS
            listaSonsPrincipais.forEach(function(element) {
                $('#selectPrincipal').append(
                "<option value='"+
                element.idSom+
                "'>"+
                element.nomeSom+
                "</option>"
                );
            }, this);
            
            // ATIVA O SELECTPICKER
            $('.selectpicker').selectpicker({
                style: 'btn-default',
                width: 'auto',
                title: 'Selecione...',
                dropupAuto: false
            });

            // ATIVA AS TOOLTIPS
            $('[data-toggle="tooltip"]').tooltip();

            // ATIVA OS SLIDERS
            $('[data-slider-max=100]').bootstrapSlider({
                formatter: function(value) {
                    return 'Volume: ' + value;
                }
            });

            // create web audio api context
            var AudioContext = window.AudioContext || window.webkitAudioContext;
            var audioCtx = new AudioContext();
            var startingGain = 0.5;
            var startingPan = 0;

            // WEB AUDIO API RUIDO A
            var audioRuidoA = $("#audioRuidoA")[0];
            audioRuidoA.volume = 1;
            var sourceRuidoA = audioCtx.createMediaElementSource(audioRuidoA);
            var gainNodeRuidoA = audioCtx.createGain();
            var panNodeRuidoA = audioCtx.createStereoPanner();
            gainNodeRuidoA.gain.value = startingGain;
            panNodeRuidoA.pan.value = startingPan;
            $("input#volumeRuidoA").bootstrapSlider('setValue',startingGain * 100);
            sourceRuidoA.connect(panNodeRuidoA);
            panNodeRuidoA.connect(gainNodeRuidoA);
            gainNodeRuidoA.connect(audioCtx.destination);
            
            // WEB AUDIO API PRINCIPAL
            var audioPrincipal = $("#audioPrincipal")[0];
            audioPrincipal.volume = 1;
            var sourcePrincipal = audioCtx.createMediaElementSource(audioPrincipal);
            var gainNodePrincipal = audioCtx.createGain();
            var panNodePrincipal = audioCtx.createStereoPanner();
            gainNodePrincipal.gain.value = startingGain;
            panNodePrincipal.pan.value = startingPan;
            $("input#volumePrincipal").bootstrapSlider('setValue',startingGain * 100);
            sourcePrincipal.connect(panNodePrincipal);
            panNodePrincipal.connect(gainNodePrincipal);
            gainNodePrincipal.connect(audioCtx.destination);

            // WEB AUDIO API RUIDO B
            var audioRuidoB = $("#audioRuidoB")[0];
            audioRuidoB.volume = 1;
            var sourceRuidoB = audioCtx.createMediaElementSource(audioRuidoB);
            var gainNodeRuidoB = audioCtx.createGain();
            var panNodeRuidoB = audioCtx.createStereoPanner();
            gainNodeRuidoB.gain.value = startingGain;
            panNodeRuidoB.pan.value = startingPan;
            $("input#volumeRuidoB").bootstrapSlider('setValue',startingGain * 100);
            sourceRuidoB.connect(panNodeRuidoB);
            panNodeRuidoB.connect(gainNodeRuidoB);
            gainNodeRuidoB.connect(audioCtx.destination);
            
            // FUNCOES ONCLICK BALANCO RUIDO A
            $("#bntOrelhaEsquerdaRuidoA").on("click", function() {
                panNodeRuidoA.pan.value = -1;
                $("#bntOrelhaEsquerdaRuidoA").prop('disabled', true);
                $("#bntOrelhaAmbasRuidoA").prop('disabled', false);
                $("#bntOrelhaDireitaRuidoA").prop('disabled', false);
            });
            $("#bntOrelhaAmbasRuidoA").on("click", function() {
                panNodeRuidoA.pan.value = 0;
                $("#bntOrelhaEsquerdaRuidoA").prop('disabled', false);
                $("#bntOrelhaAmbasRuidoA").prop('disabled', true);
                $("#bntOrelhaDireitaRuidoA").prop('disabled', false);
            });
            $("#bntOrelhaDireitaRuidoA").on("click", function() {
                panNodeRuidoA.pan.value = 1;
                $("#bntOrelhaEsquerdaRuidoA").prop('disabled', false);
                $("#bntOrelhaAmbasRuidoA").prop('disabled', false);
                $("#bntOrelhaDireitaRuidoA").prop('disabled', true);
            });

            // FUNCOES ONCLICK BALANCO PRINCIPAL
            $("#bntOrelhaEsquerdaPrincipal").on("click", function() {
                panNodePrincipal.pan.value = -1;
                $("#bntOrelhaEsquerdaPrincipal").prop('disabled', true);
                $("#bntOrelhaAmbasPrincipal").prop('disabled', false);
                $("#bntOrelhaDireitaPrincipal").prop('disabled', false);
            });
            $("#bntOrelhaAmbasPrincipal").on("click", function() {
                panNodePrincipal.pan.value = 0;
                $("#bntOrelhaEsquerdaPrincipal").prop('disabled', false);
                $("#bntOrelhaAmbasPrincipal").prop('disabled', true);
                $("#bntOrelhaDireitaPrincipal").prop('disabled', false);
            });
            $("#bntOrelhaDireitaPrincipal").on("click", function() {
                panNodePrincipal.pan.value = 1;
                $("#bntOrelhaEsquerdaPrincipal").prop('disabled', false);
                $("#bntOrelhaAmbasPrincipal").prop('disabled', false);
                $("#bntOrelhaDireitaPrincipal").prop('disabled', true);
            });

            // FUNCOES ONCLICK BALANCO RUIDO B
            $("#bntOrelhaEsquerdaRuidoB").on("click", function() {
                panNodeRuidoB.pan.value = -1;
                $("#bntOrelhaEsquerdaRuidoB").prop('disabled', true);
                $("#bntOrelhaAmbasRuidoB").prop('disabled', false);
                $("#bntOrelhaDireitaRuidoB").prop('disabled', false);
            });
            $("#bntOrelhaAmbasRuidoB").on("click", function() {
                panNodeRuidoB.pan.value = 0;
                $("#bntOrelhaEsquerdaRuidoB").prop('disabled', false);
                $("#bntOrelhaAmbasRuidoB").prop('disabled', true);
                $("#bntOrelhaDireitaRuidoB").prop('disabled', false);
            });
            $("#bntOrelhaDireitaRuidoB").on("click", function() {
                panNodeRuidoB.pan.value = 1;
                $("#bntOrelhaEsquerdaRuidoB").prop('disabled', false);
                $("#bntOrelhaAmbasRuidoB").prop('disabled', false);
                $("#bntOrelhaDireitaRuidoB").prop('disabled', true);
            });

            // ON CHANGE DE VOLUME
            $("input#volumeRuidoA").on('change',function(){
                gainNodeRuidoA.gain.value = $("input#volumeRuidoA").bootstrapSlider('getValue')/100;
            });
            $("input#volumePrincipal").on('change',function(){
                gainNodePrincipal.gain.value = $("input#volumePrincipal").bootstrapSlider('getValue')/100;
            });
            $("input#volumeRuidoB").on('change',function(){
                gainNodeRuidoB.gain.value = $("input#volumeRuidoB").bootstrapSlider('getValue')/100;
            });

            $("#botaoPlay").on("click",function (){
                $("#audioRuidoA")[0].play();
                $("#audioPrincipal")[0].play();
                $("#audioRuidoB")[0].play();
            });
            $("#botaoPause").on("click",function (){
                $("#audioRuidoA")[0].pause();
                $("#audioPrincipal")[0].pause();
                $("#audioRuidoB")[0].pause();
            });
            $("#botaoStop").on("click",function (){
                $("#audioRuidoA")[0].pause();
                $("#audioPrincipal")[0].pause();
                $("#audioRuidoB")[0].pause();
                $("#audioRuidoA")[0].load();
                $("#audioPrincipal")[0].load();
                $("#audioRuidoB")[0].load();
            });
            
        });
        </script>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>
                        
            <!-- PARTE SUPERIOR -->
            <div class="row linha">

                <!-- ESQUERDA - CONTROLES GERAIS -->
                <div class="col-md-3">
                    <br><br><br>
                    <div style="text-align:center;">
                        <button id="botaoPlay" class="btn btn-default" data-toggle="tooltip" title="Tocar todos os sons">
                            <i class="fa fa-play fa-3x"></i>
                        </button>
                        
                        <button id="botaoPause" class="btn btn-default" data-toggle="tooltip" title="Pausar todos os sons">
                            <i class="fa fa-pause fa-3x"></i>
                        </button>

                        <button id="botaoStop" class="btn btn-default" data-toggle="tooltip" title="Parar todos os sons">
                            <i class="fa fa-stop fa-3x"></i>
                        </button>
                    </div>
                </div>

                <!-- CENTRO - IMAGEM -->
                <div class="col-md-6 background" style="border: 1px solid black;">
                    <img class="block marginCentering moldura" src="../img/tv2.png" alt="">
                </div>

                <!-- DIREITA - DADOS DO PACIENTE -->
                <div class="col-md-3">
                    <div style="text-align:right;"><button class="btn btn-default" onclick="mostrarEsconderControles(500);">Mostrar!</button></div>
                    
                </div>
            </div>

            <div class="row linha">
                <!-- COLUNA 1 SOM RUIDO -->
                <div class="col-md-4 textAlignCentering">

                    <!-- SELECT COM AUDIOS -->
                    <label id="labelRuidoA" for="selectRuidoA">Ruído A</label>
                    <br>
                    <select id='selectRuidoA' class='selectpicker dropup'>
                    <option value=''></option>
                    </select><br><br>

                    <!-- VOLUME -->
                    <input 
                        id="volumeRuidoA" 
                        type="text" 
                        data-slider-id='volumeRuidoA' 
                        data-slider-min="0" 
                        data-slider-max="100" 
                        data-slider-step="1" 
                        data-slider-value="75"/>
                    <br><br>

                    <!-- BALANÇO  -->
                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha esquerda" id="bntOrelhaEsquerdaRuidoA">
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror">                        
                    </button>
                    <button class="btn btn-default" data-toggle="tooltip" title="Som em ambas as orelhas" id="bntOrelhaAmbasRuidoA" disabled>
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror"> 
                        <img src="../img/ear.svg" alt="orelhas direita" class="orelha">
                    </button>
                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha direita" id="bntOrelhaDireitaRuidoA">
                        <img src="../img/ear.svg" alt="orelha direita" class="orelha">
                    </button>
                    <br><br>
                    
                    <audio id="audioRuidoA">
                        <source type='audio/mpeg'>
                        Your browser does not support the audio element.
                    </audio>
                </div>

                <!-- COLUNA 2 SOM PRINCIPAL -->
                <div class="col-md-4 textAlignCentering">
                    
                    <!-- SELECT COM AUDIOS -->
                    <label id="labelPrincipal" for="selectPrincipal">Instrução</label><br>
                    <select id='selectPrincipal' class='selectpicker dropup'>
                    <option value=''></option>
                    </select><br><br>

                    <!-- VOLUME -->
                    <input 
                        id="volumePrincipal" 
                        type="text" 
                        data-slider-id='volumePrincipal' 
                        data-slider-min="0" 
                        data-slider-max="100" 
                        data-slider-step="1" 
                        data-slider-value="75"/>
                    <br><br>

                    <!-- BALANÇO  -->
                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha esquerda" id="bntOrelhaEsquerdaPrincipal">
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror">                        
                    </button>

                    <button class="btn btn-default" data-toggle="tooltip" title="Som em ambas as orelhas" id="bntOrelhaAmbasPrincipal" disabled>
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror"> 
                        <img src="../img/ear.svg" alt="orelhas direita" class="orelha">
                    </button>

                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha direita" id="bntOrelhaDireitaPrincipal">
                        <img src="../img/ear.svg" alt="orelha direita" class="orelha">
                    </button>
                    <br><br>

                    <audio id="audioPrincipal">
                        <source type='audio/mpeg'>
                        Your browser does not support the audio element.
                    </audio>
                </div>

                <!-- COLUNA 3 SOM RUIDO -->
                <div class="col-md-4 textAlignCentering">

                    <!-- SELECT COM AUDIOS -->
                    <label id="labelRuidoB" for="selectRuidoB">Ruído B</label><br>
                    <select id='selectRuidoB' class='selectpicker dropup'>
                    <option value=''></option>
                    </select><br><br>

                    <!-- VOLUME -->
                    <input 
                        id="volumeRuidoB" 
                        type="text" 
                        data-slider-id='volumeRuidoB' 
                        data-slider-min="0" 
                        data-slider-max="100" 
                        data-slider-step="1" 
                        data-slider-value="75"/>
                    <br><br>

                    <!-- BALANÇO  -->
                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha esquerda" id="bntOrelhaEsquerdaRuidoB">
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror">                        
                    </button>

                    <button class="btn btn-default" data-toggle="tooltip" title="Som em ambas as orelhas" id="bntOrelhaAmbasRuidoB" disabled>
                        <img src="../img/ear.svg" alt="orelha esquerda" class="orelha mirror"> 
                        <img src="../img/ear.svg" alt="orelhas direita" class="orelha">
                    </button>

                    <button class="btn btn-default" data-toggle="tooltip" title="Som na orelha direita" id="bntOrelhaDireitaRuidoB">
                        <img src="../img/ear.svg" alt="orelha direita" class="orelha">
                    </button>
                    <br><br>

                    <audio id="audioRuidoB">
                        <source type='audio/mpeg'>
                        Your browser does not support the audio element.
                    </audio>
                </div>
            </div>
            
            <!-- MENSAGENS DE STATUS -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    exibeResultadoOuErro();
                    ?>                    
                </div>
            </div>
            <br><br>

        </div>
    </body>
</html>