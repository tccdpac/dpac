<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioAutenticado();

?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
        
        </style>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>
        
        <section class="container-fluid divCentralizada">
            <form action="../controller/cadastrarUsuario.controller.php" method="post">
                <div class="row linha">
                    <div class="col-md-12">
                        <h3>Cadastro de Usuário:</h3>
                    </div>
                </div>

                <!-- NOME COMPLETO -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input class="form-control" type="text" name="nome" placeholder="Nome Completo" required>
                        </div>
                    </div>
                </div>

                <!-- EMAIL -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input class="form-control" type="email" name="email" placeholder="E-mail" required>
                        </div>
                    </div>
                </div>

                <!-- SENHA 1 -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input class="form-control" type="password" name="senha1" placeholder="Senha" required>
                        </div>
                    </div>
                </div>

                <!-- SENHA 2 -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-repeat"></i></span>
                            <input class="form-control" type="password" name="senha2" placeholder="Repita sua senha" required>
                        </div>
                    </div>
                </div>
                <br>

                <!-- CADASTRAR -->
                <div class="row linha">
                    <div class="col-md-12">
                        <button class="btn btn-primary" style="width: 100%" type="submit">
                            <i class="glyphicon glyphicon-floppy-disk"></i>
                            Cadastrar
                        </button>
                    </div>
                </div>

                <!-- MENSAGENS DE STATUS -->
                <div class="row linha">
                    <div class="col-md-12">
                        <?php
                        exibeResultadoOuErro();
                        ?>     
                    </div>
                </div>

            </form>

            <!-- VOLTAR -->
            <div class="row linha">
                <div class="col-md-12">
                    <a href="login.view.php">
                        <button class="btn btn-default" style="width: 30%" type="submit">
                            <i class="glyphicon glyphicon-arrow-left"></i>
                            Voltar
                        </button>
                    </a>
                </div>
            </div>
        </section>
    </body>
</html>