<?php
require_once "../model/Paciente.class.php";
require_once "../model/Responsavel.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

// <!-- DADOS DO PACIENTE -->
if(isset($_SESSION["paciente"])){
    $paciente = $_SESSION["paciente"];
    unset($_SESSION["paciente"]);
} else {
    echo "Erro! Paciente não encontrado!";
}
if(isset($_SESSION["responsaveis"])){
    $responsaveis = $_SESSION["responsaveis"];
    unset($_SESSION["responsaveis"]);
} else {
    echo "Erro! Responsável não encontrado!";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <script>
            function toggleConfirmaExcluir (tempo) {
                $("#confirmaExcluir").toggle(tempo);
            }
            function adicionaCampoResponsavel () {
                //verifica a quantidade de responsaveis atual
                var quantidadeResponsaveis = $("#quantidadeResponsaveis").prop('value');
                //adiciona mais um
                quantidadeResponsaveis = parseInt(quantidadeResponsaveis) + 1;
                //recoloca no contador
                $("#quantidadeResponsaveis").prop('value', quantidadeResponsaveis);
                //seta o html de uma linha
                var linhaResponsavel = 
                '<div class="row linha" id="linhaResponsavel'+quantidadeResponsaveis+'">'+
                    '<input type="hidden" name="idResponsavel[]" id="idLinhaResponsavel'+quantidadeResponsaveis+'">'+
                    '<div class="col-md-6">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon"><i class="fa fa-user"></i></span>'+
                            '<input class="form-control" type="text" name="nomeResponsavel[]" placeholder="Nome do Responsável" id="nomeLinhaResponsavel'+quantidadeResponsaveis+'">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-5">'+
                        '<div class="input-group">'+
                            '<span class="input-group-addon"><i class="fa fa-phone"></i></span>'+
                            '<input class="form-control" type="text" maxlength="20" name="telefoneResponsavel[]" placeholder="Telefone do Responsável" id="telefoneLinhaResponsavel'+quantidadeResponsaveis+'">'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                        '<div class="input-group">'+
                            '<a style="display: none;" class="btn btn-default" href="" onclick="" id="linkRemoverResponsavel'+quantidadeResponsaveis+'">'+
                                '<i class="fa fa-close"></i>'+
                            '</a>'+
                        '</div>'+
                    '</div>'+
                '</div>';
                $("#responsaveis").append(linhaResponsavel);
            }

            function removeCampoResponsavel () {
                //verifica a quantidade de responsaveis atual, que significa também o numero do ultimo responsavel
                var quantidadeResponsaveis = $("#quantidadeResponsaveis").prop('value');
                if(0 < parseInt(quantidadeResponsaveis)){
                    //remove o responsavel
                    $("#linhaResponsavel"+quantidadeResponsaveis).remove();
                    //remove um
                    quantidadeResponsaveis = parseInt(quantidadeResponsaveis) - 1;
                    //recoloca no contador
                    $("#quantidadeResponsaveis").prop('value', quantidadeResponsaveis);
                }
            }
            function toggleFormNovoRegistro($duracao){
                $("#divFormNovoRegistro").toggle($duracao);
            }
            $( document ).ready(function() {
                ativaDatePicker();
                toggleFormNovoRegistro(0);
                //trecho de codigo que cria e popula as linhas de responsáveis
                <?php
                foreach ($responsaveis as $key => $responsavel) {
                    $idPaciente = $responsavel->getIdPaciente();
                    $idResponsavel = $responsavel->getIdResponsavel();
                    $nomeResponsavel = $responsavel->getNomeResponsavel();
                    $numeroTelefoneResponsavel = $responsavel->getNumeroTelefoneResponsavel();
                    $linkRemoverResponsavel = "../controller/removerLinhaResponsavel.controller.php?idPaciente=$idPaciente&idResponsavel=$idResponsavel";
                    ?>

                    adicionaCampoResponsavel();
                    var quantidadeResponsaveis = $("#quantidadeResponsaveis").prop('value');
                    $('#idLinhaResponsavel'+quantidadeResponsaveis).prop('value', '<?= $idResponsavel ?>');
                    $('#nomeLinhaResponsavel'+quantidadeResponsaveis).prop('value', '<?= $nomeResponsavel ?>');
                    $('#telefoneLinhaResponsavel'+quantidadeResponsaveis).prop('value', '<?= $numeroTelefoneResponsavel ?>');
                    $('#linkRemoverResponsavel'+quantidadeResponsaveis).prop('href', '<?= $linkRemoverResponsavel ?>');
                    $('#linkRemoverResponsavel'+quantidadeResponsaveis).css('display', 'inline-block');
                <?php
                }
                ?>
            });
        </script>
        <style>
        .color {
            border: red;
        }
        /* a {
            color: black;
        } */
        </style>  
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>            
                        
            <div class="row linha">
                <div class="col-md-12">
                    <!-- CADASTRO DE PACIENTE -->
                    <div class="container-fluid divCadastroPaciente">
                        
                        <!-- BOTÃO VOLTAR E EXCLUIR -->
                        <div class="row linha">
                            <div class="col-md-12">
                                <a href="../controller/listaPaciente.controller.php" class="btn btn-default">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                                <a href="#" onclick="toggleConfirmaExcluir(200)" class="btn btn-default" style="float:right;">
                                    <i class='fa fa-trash' aria-hidden='true'></i> Excluir
                                </a>       
                            </div>
                        </div>

                        <div class="row linha">
                            <div class="col-md-12">
                                <div style='display: inline-block; float: right;'>
                                    <span id='confirmaExcluir' style='display: none;'><br>
                                        Confirma a exclusão? 
                                        <a href='#' class='btn btn-default' onclick='toggleConfirmaExcluir(200)'>Não</a>
                                        <a href='../controller/excluirPaciente.controller.php?idPaciente=<?= $paciente->getIdPaciente() ?>' class='btn btn-default'>Sim</a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <form id="formCadastroPaciente" method="post" action="../controller/atualizarPaciente.controller.php">
                            <input type="hidden" name="idUsuario" value="<?php echo $_SESSION['idUsuario']?>">
                            <input type="hidden" name="idPaciente" value="<?= $paciente->getIdPaciente() ?>">
                            
                            <!-- NOME -->
                            <div class="row linha">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input value="<?= $paciente->getNomePaciente() ?>" class="form-control" type="text" name="nomePaciente" placeholder="Nome Completo" required>
                                    </div>
                                </div>
                            </div>

                            <!-- DATA NASCIMENTO -->
                            <div class="row linha">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input value="<?= $paciente->getDataNascPaciente() ?>" class="form-control datepicker" type="text" name="dataNascPaciente" placeholder="Data de Nascimento">
                                    </div>
                                </div>
                            </div>

                            <div class="row linha">
                                <!-- NOME DOC -->
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-id-card"></i></span>
                                        <input value="<?= $paciente->getNomeDocIdentificacaoPaciente() ?>" class="form-control" type="text" name="nomeDocIdentificacaoPaciente" placeholder="Tipo de Documento">
                                    </div>
                                </div>

                                <!-- NUM DOC -->
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                                        <input value="<?= $paciente->getNumDocIdentificacaoPaciente() ?>" class="form-control" type="text" name="numDocIdentificacaoPaciente" placeholder="Número do documento">
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row linha">
                                <!-- NATURALIDADE -->
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                        <input value="<?= $paciente->getNaturalidadePaciente() ?>" class="form-control" type="text" name="naturalidadePaciente" placeholder="Naturaliade">
                                    </div>
                                </div>

                                <!-- NACIONALIDADE -->
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
                                        <input value="<?= $paciente->getNacionalidadePaciente() ?>" class="form-control" type="text" name="nacionalidadePaciente" placeholder="Nacionalidade">
                                    </div>
                                </div>
                            </div>

                            <!-- OBSERVACAO -->
                            <div class="row linha">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-comment"></i></span>
                                        <textarea class="form-control" type="text" name="observacaoPaciente" placeholder="Observação" rows="3"><?= $paciente->getObservacaoPaciente() ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <!-- RESPONSÁVEL -->
                            <div id="responsaveis">
                                
                            </div>
                            <br>
                            <div class="row linha">
                                <div class="col-md-6">
                                    <button class="btn btn-default" style="width: 50px;" type="button" onclick="adicionaCampoResponsavel();"><i class="fa fa-plus"></i></button>
                                    <button class="btn btn-default" style="width: 50px;" type="button" onclick="removeCampoResponsavel();"><i class="fa fa-minus"></i></button>
                                    <input id="quantidadeResponsaveis" type="hidden" value="0">
                                </div>
                                
                                <!-- SUBMIT -->
                                <div class="col-md-6">
                                    <button class="btn btn-success d-inline-block pull-right" type="submit">
                                        <i class="fa fa-save"></i>
                                        Atualizar
                                    </button>
                                </div>

                            </div>
                            <br>

                        </form>

                        <!-- MENSAGENS DE STATUS -->
                        <div class="row linha">
                            <div class="col-md-12">
                                <?php
                                exibeResultadoOuErro();
                                ?>     
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>