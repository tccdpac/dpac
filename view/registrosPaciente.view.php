<?php
require_once "../model/Paciente.class.php";
require_once "../model/Responsavel.class.php";
require_once "../model/Registro.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

// <!-- DADOS DO PACIENTE -->
if(isset($_SESSION["paciente"])){
    $paciente = $_SESSION["paciente"];
    unset($_SESSION["paciente"]);
} else {
    echo "Erro! Paciente não encontrado!";
}
if(isset($_SESSION["responsaveis"])){
    $responsaveis = $_SESSION["responsaveis"];
    unset($_SESSION["responsaveis"]);
} else {
    echo "Erro! Responsável não encontrado!";
}
if(isset($_SESSION["registros"])){
    $registros = $_SESSION["registros"];
    unset($_SESSION["registros"]);
} else {
    echo "Erro! Responsável não encontrado!";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <script>
            function toggleFormNovoRegistro(duracao){
                $("#divFormNovoRegistro").toggle(duracao);
            }
            function toggleEdicao (duracao, numero) {
                $("#botaoEditar"+numero).toggle(duracao);
                $("#botaoExcluir"+numero).toggle(duracao);                
                $("#spanA"+numero).toggle(duracao);
                $("#spanB"+numero).toggle(duracao);

                $("#botaoCancelar"+numero).toggle(duracao);
                $("#botaoSalvar"+numero).toggle(duracao);
                $("#input"+numero).toggle(duracao);
                $("#textarea"+numero).toggle(duracao);
            }
            function toggleConfirmacao (duracao, numero) {
                $("#boxConfirmacao"+numero).toggle(duracao);
            }
            $( document ).ready(function() {
                ativaDatePicker();
                toggleFormNovoRegistro(0);
            });
        </script>
        <style>
        .color {
            border: red;
        }
        .table > tbody > tr > td:first-child{
            width: 120px;
        }
        
        .table > tbody > tr > td:nth-child(3){
            width: 120px;
        }
        .titulo {
            font-weight: bold;
            font-size: 20px;
        }
        </style>
        <script>
        
        </script>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>            
                        
            <div class="row linha">
                
                <!-- BLOCO DE REGISTROS -->
                <div class="col-md-12">
                    <div class="container-fluid divCadastroPaciente">
                        
                        <!-- BOTÃO VOLTAR E NOVO REGISTRO -->
                        <div class="row linha">
                            <div class="col-md-3">
                                <a href="../controller/listaPaciente.controller.php" class="btn btn-default pull-left">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                            </div>
                            <div class="col-md-6" style="text-align: center;">
                                <span class="titulo">REGISTROS DO PACIENTE</span>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <br>

                        <div class="row linha">
                            <div class="col-md-12">
                                <div style="border-left: 1px solid #bbb; padding-left: 5px; text-align: justify;">
                                    <span style="font-weight: bold;">Nome Completo:</span> <?= $paciente->getNomePaciente() ?><br><br>
                                    <span style="font-weight: bold;">Observação:</span> <span style=""><?= $paciente->getObservacaoPaciente() ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row linha">
                            <div class="col-md-12">
                                <button class="btn btn-default pull-right" onclick="toggleFormNovoRegistro(400)">Novo Registro <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div id="divFormNovoRegistro">
                            <form action="../controller/cadastrarRegistro.controller.php" method="post">

                                <input type="hidden" name="idPaciente" value="<?= $paciente->getIdPaciente() ?>">
                                <!-- DESCRICAO REGISTRO -->
                                <div class="row linha">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                            <textarea class="form-control" type="text" name="descricaoRegistro" placeholder="DescricaoRegistro" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- DATA REGISTRO -->
                                <div class="row linha">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <!-- <label for="dataRegistro">Data Registro:</label> -->
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input style="width: 100px;" value="<?= dataHoje() ?>" class="form-control datepicker" type="text" name="dataRegistro" placeholder="Data Registro">
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <button type="button" class="btn btn-default pull-right" onclick="toggleFormNovoRegistro(400)"><i class="fa fa-arrow-up"></i> Cancelar</button>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <button class="btn btn-success d-inline-block pull-right" type="submit"><i class="fa fa-save"></i> Registrar</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                        
                        <?php
                        if($registros != null){
                            echo '
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>DATA</th>
                                    <th>DESCRIÇÃO</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                            
                            $idPaciente = $paciente->getIdPaciente();
                            foreach ($registros as $key => $registro) {
                                $idRegistro = $registro->getIdRegistro();
                                $dataRegistro = $registro->getDataRegistro();
                                $descricaoRegistro = $registro->getDescricaoRegistro();
                                echo "
                                <form method='post' action='../controller/atualizarRegistro.controller.php'>
                                <tr>
                                    <td>
                                        <span id='spanA$idRegistro'>$dataRegistro</span>
                                        <input type='hidden' name='idRegistro' value='$idRegistro'>
                                        <input type='hidden' name='idPaciente' value='$idPaciente'>
                                        <input id='input$idRegistro' style='display:none' value='$dataRegistro' class='form-control datepicker' type='text' name='dataRegistro'>
                                    </td>
                                    <td>
                                        <span id='spanB$idRegistro'>$descricaoRegistro</span>
                                        <textarea id='textarea$idRegistro' style='display:none' class='form-control' name='descricaoRegistro' rows='3'>$descricaoRegistro</textarea>
                                        <div style='display:none; text-align: center;' id='boxConfirmacao$idRegistro'>
                                            <br>
                                            Confirma a remoção do registro? 
                                            <a class='btn btn-default' onclick='toggleConfirmacao(400,$idRegistro)'>Não</a> 
                                            <a class='btn btn-default' href='../controller/excluirRegistro.controller.php?idRegistro=$idRegistro&idPaciente=$idPaciente'>Sim</a>
                                            <br>
                                        </div>
                                    </td>
                                    <td>
                                        <a id='botaoEditar$idRegistro' class='btn btn-default' href='#' onclick='toggleEdicao(400,$idRegistro)'><i class='fa fa-pencil'></i></a>
                                        <a id='botaoExcluir$idRegistro' class='btn btn-default' href='#' onclick='toggleConfirmacao(400,$idRegistro)'><i class='fa fa-trash'></i></a>

                                        <a style='display:none;' id='botaoCancelar$idRegistro' class='btn btn-default' href='#' onclick='toggleEdicao(400,$idRegistro)'><i class='fa fa-times'></i></a>
                                        <button style='display:none;' id='botaoSalvar$idRegistro' class='btn btn-default' href='#' type='submit'><i class='fa fa-save'></i></button>
                                    </td>
                                </tr>
                                </form>
                                ";                                
                            }

                            echo '
                            </tbody>
                            </table>
                            ';
                        } else {
                            echo "
                            <div class='row linha'>
                                <div class='col-md-12'>
                                    Este paciente ainda não possui registros.    
                                </div>
                            </div>
                            ";
                        }
                        ?>

                        <!-- MENSAGENS DE STATUS -->
                        <div class="row linha">
                            <div class="col-md-12">
                                <?php
                                exibeResultadoOuErro();
                                ?>     
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>