<?php
require_once "../model/Paciente.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
            .table > tbody > tr > td:nth-child(3) {
                max-width: 100px;
            }
        </style>
        <script>
        function toggleConfirmaExcluir (idPaciente, tempo) {
            $("#confirmaExcluir"+idPaciente).toggle(tempo);
        }
        </script>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid">
            
            <!-- MENU -->
            <?php require_once "../include/nav.inc"; ?>
            
            <div class="textAlignCentering">
                <a class="btn btn-primary" href="cadastroPaciente.view.php">
                    Cadastrar <i class='fa fa-plus' aria-hidden='true'></i>
                </a>
            </div>
            
            <!-- LISTA DE PACIENTES -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    if(isset($_SESSION["listaPacientes"])){
                        $listaPacientes = $_SESSION["listaPacientes"];
                        unset($_SESSION["listaPacientes"]);

                        echo "
                        <table class='table marginCentering'>
                            <tr>
                                <th>#Pac.</th>
                                <th>Nome Completo</th>
                                <th>Funções</th>
                            </tr>
                        ";
                        foreach ($listaPacientes as $paciente) {
                            $idPaciente = $paciente->getIdPaciente();
                            $nomePaciente = $paciente->getNomePaciente();
                            echo "
                            <tr>
                                <td>$idPaciente</td>
                                <td>$nomePaciente</td>
                                <td>
                                    <div class='btn-group'>
                                        <a style='width:70px;' href='../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente' aria-label='Editar' class='btn btn-default'>
                                            <i class='fa fa-pencil-square-o fa-2x' aria-hidden='true'></i>
                                        </a>
                                        <a style='width:70px;'href='../controller/mostraRegistrosPaciente.controller.php?idPaciente=$idPaciente' aria-label='Registros' class='btn btn-default'>
                                            <i class='fa fa-address-book-o fa-2x' aria-hidden='true'></i>
                                        </a>
                                        <a style='width:70px;' onclick='toggleConfirmaExcluir($idPaciente,200)' href='#' aria-label='Excluir' class='btn btn-default'>
                                            <i class='fa fa-trash-o fa-2x' aria-hidden='true'></i>
                                        </a>
                                    </div>
                                    <div style='display: inline-block;'>
                                        <span id='confirmaExcluir$idPaciente' style='display: none;'><br>
                                            Confirma a exclusão? 
                                            <a href='#' class='btn btn-default' onclick='toggleConfirmaExcluir($idPaciente,200)'>Não</a>
                                            <a href='../controller/excluirPaciente.controller.php?idPaciente=$idPaciente' class='btn btn-default'>Sim</a>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            ";
                        }
                        echo "</table>";
                    } else {
                        echo "Você ainda não tem Pacientes cadastrados!";
                    }
                    ?>
                </div>
            </div>

            <!-- MENSAGENS DE STATUS -->
            <div class="row linha">
                <div class="col-md-12 table marginCentering" style="float:none;">
                    <?php
                    exibeResultadoOuErro();
                    ?>     
                </div>
            </div>
            <br><br>

        </div>
    </body>
</html>