<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioAutenticado();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once "../include/head.inc"; ?>
        <style>
        
        </style>
    </head>
    <body>
        <?php require_once "../include/header.inc"; ?>

        <div class="container-fluid divCentralizada">
            <form action="../controller/login.controller.php" method="post">
                <!-- LOGIN -->
                <div class="row linha">
                    <div class="col-md-8">
                        <h3>Login:</h3>
                    </div>
                </div>

                <!-- EMAIL -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input class="form-control" type="email" name="email" placeholder="E-mail" required>
                        </div>
                    </div>
                </div>

                <!-- SENHA -->
                <div class="row linha">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input class="form-control" type="password" name="senha" placeholder="Senha" required>
                        </div>
                    </div>
                </div>

                <!-- ENTRAR -->
                <div class="row linha">
                    <div class="col-md-12">
                        <button class="btn btn-success" style="width: 100%" type="submit">
                            <i class="glyphicon glyphicon-log-in"></i>
                            Entrar
                        </button>
                    </div>
                </div>
            </form>
            <!-- MENSAGENS DE STATUS -->
            <div class="row linha">
                <div class="col-md-12">
                    <?php
                    exibeResultadoOuErro();
                    ?>     
                </div>
            </div>
            <br><br>

            <!-- CADASTRAR -->
            <div class="row linha">
                <div class="col-md-12 textAlignCentering">
                    Não tem um acesso?<br>
                    <a href="cadastro.view.php">
                        <button class="btn btn-primary" style="width: 100%">
                            <i class="glyphicon glyphicon-user"></i>
                            Cadastre-se
                        </button>
                    </a>
                </div>
            </div>            
        </div>
    </body>
</html>