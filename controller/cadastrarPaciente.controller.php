<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_POST["idUsuario"];
$nomePaciente = $_POST["nomePaciente"];
$dataNascPaciente =  $_POST["dataNascPaciente"];
$nomeDocIdentificacaoPaciente = $_POST["nomeDocIdentificacaoPaciente"];
$numDocIdentificacaoPaciente = $_POST["numDocIdentificacaoPaciente"];
$naturalidadePaciente = $_POST["naturalidadePaciente"];
$nacionalidadePaciente = $_POST["nacionalidadePaciente"];
$observacaoPaciente = $_POST["observacaoPaciente"];
$nomesResponsaveis = $_POST["nomeResponsavel"];
$telefonesResponsaveis = $_POST["telefoneResponsavel"];

$dataValida = validaData($dataNascPaciente);
if( !$dataValida[0] ){
    phpRedireciona("../view/cadastroPaciente.view.php?erro=$dataValida[1]");
}

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";
require_once "../model/Responsavel.class.php";
require_once "../dao/ResponsavelDao.class.php";

$paciente = new Paciente;
$paciente->setIdUsuario($idUsuario);
$paciente->setNomePaciente($nomePaciente);
$paciente->setDataNascPaciente($dataNascPaciente);
$paciente->setNomeDocIdentificacaoPaciente($nomeDocIdentificacaoPaciente);
$paciente->setNumDocIdentificacaoPaciente($numDocIdentificacaoPaciente);
$paciente->setNaturalidadePaciente($naturalidadePaciente);
$paciente->setNacionalidadePaciente($nacionalidadePaciente);
$paciente->setObservacaoPaciente($observacaoPaciente);

$resultadoDaInsercao = PacienteDao::inserirPaciente($paciente);

if('true' == $resultadoDaInsercao){

    $resultadoDaBusca = PacienteDao::buscaPacientePorNome($nomePaciente, $idUsuario);
    
    if('true' == $resultadoDaBusca[0]){
        for($i = 0; $i < sizeof($nomesResponsaveis); $i++){
            $responsavel = new Responsavel();
            $responsavel->setIdPaciente($resultadoDaBusca[1]->getIdPaciente());
            $responsavel->setNomeResponsavel($nomesResponsaveis[$i]);
            $responsavel->setNumeroTelefoneResponsavel($telefonesResponsaveis[$i]);
            $resultadoDaInsercao = ResponsavelDao::inserirResponsavel($responsavel);
            if('true' == $resultadoDaInsercao){
                //do nothing
            } else {
                $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
                phpRedireciona("../view/cadastroPaciente.view.php?erro=$string");
            }
        }
        phpRedireciona("../view/cadastroPaciente.view.php?resultado=6");

    } else {
        //TODO a ideia aqui é nunca dar erro
    }    
} else {
    $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
    phpRedireciona("../view/cadastroPaciente.view.php?erro=$string");
}