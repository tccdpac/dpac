<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$idSom = $_GET["idSom"];

require_once "../model/Som.class.php";
require_once "../dao/SomDao.class.php";
require_once "../dao/ImagemDao.class.php";

$somPossuiImagem = ImagemDao::buscaImagemDoSom($idSom);
$imagem = $somPossuiImagem[1];


var_dump($somPossuiImagem); // retorna [0]true e [1]false


// busca o som
$som = SomDao::buscaSomPorId($idSom, $idUsuario);
//monta o caminho
$caminho = "..\\".$som[1][0]->getCaminhoArquivoSom() . $som[1][0]->getNomeSom();


$resultadoDaExclusao = SomDao::excluirSom($idSom);

if($resultadoDaExclusao) {
    
    // se deu certo, exclui o som
    var_dump(unlink($caminho));

    // se tem imagem
    if( ($somPossuiImagem[0]) && ($imagem != false) ){

        //exclui a imagem do banco.
        $idImagem = $imagem->getIdImagem();        
        $resultadoImagem = ImagemDao::excluirImagem($idImagem);

        if($resultadoImagem){
            // se deu certo, exclui a imagem
            $caminhoImagem = "..\\".$imagem->getCaminhoArquivoImagem() . $imagem->getNomeImagem();
            var_dump(unlink($caminhoImagem));

            //retorna positivo
            phpRedireciona("../controller/listaSom.controller.php?resultado=12");
        } else {
            $string =  "C1:%20$resultadoImagem[0]%20C2:%20$resultadoImagem[1]%20M1:%20$resultadoImagem[2]";
            phpRedireciona("../controller/listaSom.controller.php?erro=$string");
        }
    } else {
        // se não, só retorna positivo
        phpRedireciona("../controller/listaSom.controller.php?resultado=11");
    }
} else {
    $string =  "C1:%20$resultadoDaExclusao[0]%20C2:%20$resultadoDaExclusao[1]%20M1:%20$resultadoDaExclusao[2]";
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?erro=$string");
}