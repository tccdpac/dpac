<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];
$tipoDeSom = $_REQUEST['tipoDeSom'];

// se tipo do som é principal e não existe arquivo
if( ($tipoDeSom === 'principal') && ($_FILES['inputImagem']['error'] == 4) ){
    //retorna informando que não foi selecionada imagem
    phpRedireciona("../view/uploadDeSom.view.php?resultado=Som%20principal%20sem%20imagem");
}

require_once "../model/Som.class.php";
require_once "../dao/SomDao.class.php";
require_once "../model/Imagem.class.php";
require_once "../dao/ImagemDao.class.php";

// diretório atual
//echo getcwd() . "\n";
// checar upload_max_filesize e post_max_size no php.ini
//var_dump(ini_get('upload_max_filesize'));
//var_dump($_FILES);


// Passo 1: Verificar se já existe diretório de Som no nome deste usuário
// posiciona no diretorio
chdir('..\\Sons');

//programa como diretorio o nome do usuario
$diretorio = "$nomeUsuario\\";

//captura o nome do arquivo e seta o caminho completo
$nomeDoArquivo = basename($_FILES['inputSom']['name']);
$arquivo = $diretorio . basename($_FILES['inputSom']['name']);

// se não existe o direitorio do nome do usuario
if (!file_exists($nomeUsuario)) {
    // cria o diretorio
    mkdir($nomeUsuario);
} else {
    // já existe o diretorio. então, checa se o arquivo já existe
    chdir($nomeUsuario);
    if(file_exists($nomeDoArquivo)){
        // arquivo existe, dar aviso de arquivo já existente
        phpRedireciona("../view/uploadDeSom.view.php?resultado=arquivo%20repetido");
    }
    // retorna para o diretorio acima
    chdir('..\\');
}


// Passo 2: Mover o arquivo e inserir no banco
$som = new Som();
$som->setIdUsuario($idUsuario);
$som->setCaminhoArquivoSom("\\Sons\\$diretorio");
$som->setNomeSom($nomeDoArquivo);
$som->setTipoSom($som->converteTipoSom($tipoDeSom));

//posiciona o arquivo no local definido
if (move_uploaded_file($_FILES['inputSom']['tmp_name'], $arquivo)) {
    // insere no banco
    $resultadoDaInsercao = SomDao::insereSom($som);
    if('true' == $resultadoDaInsercao){
        //foi inserido com sucesso
        
        //Passo 3: inserir imagem
        if( ($tipoDeSom === 'principal') && ($_FILES['inputImagem']['error'] == 0) ){
            //é do tipo 'principal' e possui imagem
            // PASSO 3.1: Pegar id do som que foi inserido
            $resultadoDaBusca = SomDao::buscaSomPorIdUsuarioENomeSom($som->getIdUsuario(), $som->getNomeSom());
            $idSomInserido;
            if($resultadoDaBusca){
                //deu certo, pega o id
                $idSomInserido = $resultadoDaBusca[1]->getIdSom();
            } else {
                //deu errado (improvavel), todo retorna erro
                phpRedireciona("../view/uploadDeSom.view.php?resultado=Algo%20na%20insercao%20do%20som%20deu%20errado");
            }


            // PASSO 3.2: Verificar se já existe diretório de imagem no nome deste usuário
            chdir('..\\Imagens');
            
            //programa como diretorio o nome do usuario
            $diretorio = "$nomeUsuario\\";
            
            //captura o nome do arquivo e seta o caminho completo
            $nomeDaImagem = basename($_FILES['inputImagem']['name']);
            $arquivoImagem = $diretorio . basename($_FILES['inputImagem']['name']);
            // se não existe o direitorio do nome do usuario
            if (!file_exists($nomeUsuario)) {
                // cria o diretorio
                mkdir($nomeUsuario);
            } else {
                // já existe o diretorio. então, checa se o arquivo já existe
                chdir($nomeUsuario);
                if(file_exists($nomeDaImagem)){
                    // arquivo existe, dar aviso de arquivo já existente
                    phpRedireciona("../view/uploadDeSom.view.php?resultado=Imagem%20repetida");
                }
                // retorna para o diretorio acima
                chdir('..\\');
            }

            //PASSO 3.3 Mover a Imagem e inserir no Banco
            $imagem = new Imagem();
            $imagem->setIdSom($idSomInserido);
            $imagem->setIdUsuario($idUsuario);
            $imagem->setCaminhoArquivoImagem("\\Imagens\\$diretorio");
            $imagem->setNomeImagem($nomeDaImagem);

            //posiciona o arquivo no local definido
            if (move_uploaded_file($_FILES['inputImagem']['tmp_name'], $arquivoImagem)) {
                // insere no banco
                $resultadoDaInsercao = ImagemDao::insereImagem($imagem);
                if('true' == $resultadoDaInsercao){
                    //foi inserido com sucesso
                    phpRedireciona("../view/uploadDeSom.view.php?resultado=6");
                } else {
                    // erro na inserção da imagem no banco
                    $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
                    phpRedireciona("../view/uploadDeSom.view.php?erro=$string");
                }
            }
        }
        phpRedireciona("../view/uploadDeSom.view.php?resultado=6");
    } else {
        $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
        phpRedireciona("../view/uploadDeSom.view.php?erro=$string");
    }

} else {
    // aviso erro
    phpRedireciona("../view/uploadDeSom.view.php?resultado=erro%20no%20envio");
}