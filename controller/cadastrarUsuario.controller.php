<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioAutenticado();

if(isset($_POST['nome'])){
    $nomeUsuario = $_POST['nome'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=1");
}
if(isset($_POST['email'])){
    $emailUsuario = $_POST['email'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=2");
}
if(isset($_POST['senha1'])){
    $senha1Usuario = $_POST['senha1'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=3");
}
if(isset($_POST['senha2'])){
    $senha2Usuario = $_POST['senha2'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=4");
}

if($senha1Usuario !== $senha2Usuario){
    phpRedireciona("../view/cadastro.view.php?erro=5");
}

require_once "../model/Usuario.class.php";
require_once "../dao/UsuarioDao.class.php";
$usuario = new Usuario();
$usuario->setNomeUsuario($nomeUsuario);
$usuario->setEmailUsuario($emailUsuario);
$usuario->setSenhaUsuario($senha1Usuario);

$resultadoDaInsercao = UsuarioDao::inserirUsuario($usuario);
if('true' == $resultadoDaInsercao){
    phpRedireciona("../view/cadastro.view.php?resultado=6");
} else {
    $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
    phpRedireciona("../view/cadastro.view.php?erro=$string");
}

