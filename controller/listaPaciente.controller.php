<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];

$resultadoAnterior = '';
if(isset($_GET["resultado"])){
    $resultadoAnterior = "?resultado=".$_GET["resultado"];
} elseif(isset($_GET["erro"])){
    $resultadoAnterior = "?erro=".$_GET["erro"];
}

require_once "../model/Usuario.class.php";

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";

$resultadoDaBusca = PacienteDao::buscaTodosOsPacientesDoUsuario($idUsuario);

if($resultadoDaBusca[0]){
    $listaPacientes = $resultadoDaBusca[1];
    $_SESSION['listaPacientes'] = $listaPacientes;
    phpRedireciona("../view/listaPaciente.view.php$resultadoAnterior");
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/listaPaciente.view.php?erro=$string");
}