<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idPaciente = $_POST["idPaciente"];
$idRegistro = $_POST["idRegistro"];
$descricaoRegistro = $_POST["descricaoRegistro"];
$dataRegistro =  $_POST["dataRegistro"];

$dataValida = validaData($dataRegistro);
if( !$dataValida[0] ){
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?erro=$dataValida[1]&idPaciente=$idPaciente");
}

$dataRegistro = explode("/",$dataRegistro);
$dataRegistroBanco = date("Y-m-d",mktime (0, 0, 0, $dataRegistro[1]  , $dataRegistro[0], $dataRegistro[2]));

require_once "../model/Registro.class.php";
require_once "../dao/RegistroDao.class.php";

$registro = new Registro;
$registro->setIdRegistro($idRegistro);
$registro->setDescricaoRegistro($descricaoRegistro);
$registro->setDataRegistro($dataRegistroBanco);

var_dump($registro);

$resultadoDaAtualizacao = RegistroDao::atualizarRegistro($registro);

if($resultadoDaAtualizacao){
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?resultado=10&idPaciente=$idPaciente");
} else {
    $string =  "C1:%20$resultadoDaAtualizacao[0]%20C2:%20$resultadoDaAtualizacao[1]%20M1:%20$resultadoDaAtualizacao[2]";
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?erro=$string&idPaciente=$idPaciente");
}