<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idPaciente = $_POST["idPaciente"];
$descricaoRegistro = $_POST["descricaoRegistro"];
$dataRegistro =  $_POST["dataRegistro"];

$dataValida = validaData($dataRegistro);
if( !$dataValida[0] ){
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?idPaciente=$idPaciente&erro=$dataValida[1]");
}

$dataRegistro = explode("/",$dataRegistro);
$dataRegistroBanco = date("Y-m-d",mktime (0, 0, 0, $dataRegistro[1]  , $dataRegistro[0], $dataRegistro[2]));

require_once "../model/Registro.class.php";
require_once "../dao/RegistroDao.class.php";

$registro = new Registro;
$registro->setIdPaciente($idPaciente);
$registro->setDescricaoRegistro($descricaoRegistro);
$registro->setDataRegistro($dataRegistroBanco);

var_dump($registro);

$resultadoDaInsercao = RegistroDao::inserirRegistro($registro);

if('true' == $resultadoDaInsercao){
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?idPaciente=$idPaciente&resultado=6");
} else {
    $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
    phpRedireciona("../controller/mostraRegistrosPaciente.controller.php?idPaciente=$idPaciente&erro=$string");
}
