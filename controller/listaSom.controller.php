<?php
require_once "../model/Som.class.php";
require_once "../model/Imagem.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];

$resultadoAnterior = '';
if(isset($_GET["resultado"])){
    $resultadoAnterior = "?resultado=".$_GET["resultado"];
} elseif(isset($_GET["erro"])){
    $resultadoAnterior = "?erro=".$_GET["erro"];
}

require_once "../model/Usuario.class.php";

require_once "../dao/SomDao.class.php";
require_once "../dao/ImagemDao.class.php";


$resultadoDaBusca = SomDao::buscaTodosOsSonsDoUsuario($idUsuario);

if($resultadoDaBusca[0]){
    $listaSons = $resultadoDaBusca[1];

    foreach ($listaSons as $som) {
        if($som->getTipoSom() == 2){
            $buscaImagem = ImagemDao::buscaImagemDoSom($som->getIdSom());
            if($buscaImagem[0]){
                $som->setImagem($buscaImagem[1]);
            }
        }
    }
    $_SESSION['listaSons'] = $listaSons;
    phpRedireciona("../view/listaSom.view.php$resultadoAnterior");
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/listaSom.view.php?erro=$string");
}