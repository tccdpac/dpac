<?php
require_once "../model/Usuario.class.php";
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioAutenticado();

if(isset($_POST['email'])){
    $emailUsuario = $_POST['email'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=2");
}
if(isset($_POST['senha'])){
    $senhaUsuario = $_POST['senha'];
} else {
    phpRedireciona("../view/cadastro.view.php?erro=3");
}

require_once "../model/Usuario.class.php";
require_once "../dao/UsuarioDao.class.php";
$usuario = new Usuario();
$usuario->setEmailUsuario($emailUsuario);
$usuario->setSenhaUsuario($senhaUsuario);

$resultadoDaBusca = UsuarioDao::buscaUsuarioPorEmailESenha($usuario);

if(empty($resultadoDaBusca)){
    phpRedireciona("../view/login.view.php?erro=7");
}

$_SESSION['idUsuario'] = $resultadoDaBusca->getIdUsuario();
$_SESSION['nomeUsuario'] = $resultadoDaBusca->getNomeUsuario();

phpRedireciona("../view/main.view.php");