<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];
$idPaciente = $_GET["idPaciente"];

$resultadoAnterior = '';
if(isset($_GET["resultado"])){
    $resultadoAnterior = "?resultado=".$_GET["resultado"];
} elseif(isset($_GET["erro"])){
    $resultadoAnterior = "?erro=".$_GET["erro"];
}
if(isset($_GET["resultado2"])){
    $resultadoAnterior = "?resultado2=".$_GET["resultado2"];
} elseif(isset($_GET["erro2"])){
    $resultadoAnterior = "?erro2=".$_GET["erro2"];
}

require_once "../model/Usuario.class.php";

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";
require_once "../model/Responsavel.class.php";
require_once "../dao/ResponsavelDao.class.php";
require_once "../model/Registro.class.php";
require_once "../dao/RegistroDao.class.php";


$resultadoDaBusca = PacienteDao::buscaPacienteUsuario($idPaciente, $idUsuario);

if($resultadoDaBusca[0]){
    $paciente = $resultadoDaBusca[1];

    $resultadoDaBusca = ResponsavelDao::buscarResponsaveisPorPaciente($idPaciente);
    if($resultadoDaBusca[0]){
        $responsaveis = $resultadoDaBusca[1];

        $resultadoDaBusca = RegistroDao::buscarRegistrosDoPaciente($idPaciente);
        if($resultadoDaBusca[0]){
            $registros = $resultadoDaBusca[1];
            
            for ($i=0; $i < sizeof($registros); $i++) { 
                $dataBanco = $registros[$i]->getDataRegistro();
                $dataBanco = explode("-", $dataBanco);
                $data = $dataBanco[2]."/".$dataBanco[1]."/".$dataBanco[0];
                $registros[$i]->setDataRegistro($data);
            }
            
            $_SESSION['registros'] = $registros;
            $_SESSION['responsaveis'] = $responsaveis;
            $_SESSION['paciente'] = $paciente;
            
            phpRedireciona("../view/registrosPaciente.view.php$resultadoAnterior");
        } else {
            $resultadoDaBusca = $resultadoDaBusca[1];
            $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
            phpRedireciona("../view/registrosPaciente.view.php?erro=$string");
        }
    } else {
        $resultadoDaBusca = $resultadoDaBusca[1];
        $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
        phpRedireciona("../view/registrosPaciente.view.php?erro=$string");
    }
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/registrosPaciente.view.php?erro=$string");
}