<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];

require_once "../model/Usuario.class.php";

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";

$resultadoDaBusca = PacienteDao::buscaTodosOsPacientesDoUsuario($idUsuario);

if($resultadoDaBusca[0]){
    $listaPacientes = $resultadoDaBusca[1];
    $_SESSION['listaPacientes'] = $listaPacientes;
    phpRedireciona("../view/selecionarPacienteTeste.view.php");
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/selecionarPacienteTeste.view.php?erro=$string");
}