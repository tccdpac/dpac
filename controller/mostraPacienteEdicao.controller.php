<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];
$idPaciente = $_GET["idPaciente"];

$resultadoAnterior = '';
if(isset($_GET["resultado"])){
    $resultadoAnterior = "?resultado=".$_GET["resultado"];
} elseif(isset($_GET["erro"])){
    $resultadoAnterior = "?erro=".$_GET["erro"];
}
if(isset($_GET["resultado2"])){
    $resultadoAnterior = "?resultado2=".$_GET["resultado2"];
} elseif(isset($_GET["erro2"])){
    $resultadoAnterior = "?erro2=".$_GET["erro2"];
}


require_once "../model/Usuario.class.php";

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";
require_once "../dao/ResponsavelDao.class.php";

$resultadoDaBusca = PacienteDao::buscaPacienteUsuario($idPaciente, $idUsuario);

if($resultadoDaBusca[0]){
    $paciente = $resultadoDaBusca[1];

    $resultadoDaBusca = ResponsavelDao::buscarResponsaveisPorPaciente($idPaciente);
    if($resultadoDaBusca[0]){
        $responsaveis = $resultadoDaBusca[1];
        $_SESSION['responsaveis'] = $responsaveis;
        $_SESSION['paciente'] = $paciente;
        phpRedireciona("../view/edicaoPaciente.view.php$resultadoAnterior");
    }
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/edicaoPaciente.view.php?erro=$string");
}