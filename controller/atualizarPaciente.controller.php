<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_POST["idUsuario"];
$idPaciente = $_POST["idPaciente"];
$nomePaciente = $_POST["nomePaciente"];
$dataNascPaciente =  $_POST["dataNascPaciente"];
$nomeDocIdentificacaoPaciente = $_POST["nomeDocIdentificacaoPaciente"];
$numDocIdentificacaoPaciente = $_POST["numDocIdentificacaoPaciente"];
$naturalidadePaciente = $_POST["naturalidadePaciente"];
$nacionalidadePaciente = $_POST["nacionalidadePaciente"];
$observacaoPaciente = $_POST["observacaoPaciente"];
$idsResponsaveis = $_POST["idResponsavel"];
$nomesResponsaveis = $_POST["nomeResponsavel"];
$telefonesResponsaveis = $_POST["telefoneResponsavel"];

$dataValida = validaData($dataNascPaciente);
if( !$dataValida[0] ){
    phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&erro=$dataValida[1]");
}

require_once "../model/Paciente.class.php";
require_once "../dao/PacienteDao.class.php";
require_once "../model/Responsavel.class.php";
require_once "../dao/ResponsavelDao.class.php";

$paciente = new Paciente;
$paciente->setIdUsuario($idUsuario);
$paciente->setIdPaciente($idPaciente);
$paciente->setNomePaciente($nomePaciente);
$paciente->setDataNascPaciente($dataNascPaciente);
$paciente->setNomeDocIdentificacaoPaciente($nomeDocIdentificacaoPaciente);
$paciente->setNumDocIdentificacaoPaciente($numDocIdentificacaoPaciente);
$paciente->setNaturalidadePaciente($naturalidadePaciente);
$paciente->setNacionalidadePaciente($nacionalidadePaciente);
$paciente->setObservacaoPaciente($observacaoPaciente);

var_dump($paciente);
$resultadoDaAtualizacao = PacienteDao::atualizarPaciente($paciente);

if('true' == $resultadoDaAtualizacao){

    
    for($i = 0; $i < sizeof($nomesResponsaveis); $i++){
        $responsavel = new Responsavel();
        $responsavel->setIdPaciente($paciente->getIdPaciente());
        $responsavel->setNomeResponsavel($nomesResponsaveis[$i]);
        $responsavel->setNumeroTelefoneResponsavel($telefonesResponsaveis[$i]);

        if($idsResponsaveis[$i] != ''){
            //update
            $responsavel->setIdResponsavel($idsResponsaveis[$i]);
            echo "<h3>agora eu devia dar update mas não estou por carater de teste</h3>";
            $resultadoDaAtualizacao = ResponsavelDao::atualizarResponsavel($responsavel);
            if('true' != $resultadoDaAtualizacao){
                $string =  "C1:%20$resultadoDaAtualizacao[0]%20C2:%20$resultadoDaAtualizacao[1]%20M1:%20$resultadoDaAtualizacao[2]";
                phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&erro=$string");
            }
        } else {
            //insert
            $resultadoDaInsercao = ResponsavelDao::inserirResponsavel($responsavel);
            if('true' != $resultadoDaInsercao){
                $string =  "C1:%20$resultadoDaInsercao[0]%20C2:%20$resultadoDaInsercao[1]%20M1:%20$resultadoDaInsercao[2]";
                phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&erro=$string");
            }
                
        }
    }
    phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&resultado=6");

} else {
    $string =  "C1:%20$resultadoDaAtualizacao[0]%20C2:%20$resultadoDaAtualizacao[1]%20M1:%20$resultadoDaAtualizacao[2]";
    phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&erro=$string");
}