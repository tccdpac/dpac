<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idResponsavel = $_GET["idResponsavel"];
$idPaciente = $_GET["idPaciente"];

require_once "../model/Responsavel.class.php";
require_once "../dao/ResponsavelDao.class.php";

$resultadoDaRemocao = ResponsavelDao::removerResponsavel($idResponsavel);

if($resultadoDaRemocao){
    phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&resultado=deu%20certo");
} else {
    $string =  "C1:%20$resultadoDaRemocao[0]%20C2:%20$resultadoDaRemocao[1]%20M1:%20$resultadoDaRemocao[2]";
    phpRedireciona("../controller/mostraPacienteEdicao.controller.php?idPaciente=$idPaciente&erro=$string");
}