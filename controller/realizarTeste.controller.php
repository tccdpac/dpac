<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

require_once "../model/Usuario.class.php";
require_once "../model/Paciente.class.php";
require_once "../model/Som.class.php";
require_once "../model/Imagem.class.php";

require_once "../dao/PacienteDao.class.php";
require_once "../dao/SomDao.class.php";
require_once "../dao/ImagemDao.class.php";


$idUsuario = $_SESSION["idUsuario"];
$nomeUsuario = $_SESSION["nomeUsuario"];
$idPaciente = $_GET["paciente"];


$resultadoDaBusca = PacienteDao::buscaPacienteUsuario($idPaciente, $idUsuario);

if($resultadoDaBusca[0]){
    $paciente = $resultadoDaBusca[1];
    $_SESSION['paciente'] = $paciente;

    $resultadoDaBusca = SomDao::buscaTodosOsSonsDoUsuario($idUsuario);    
    if($resultadoDaBusca[0]){
        $listaSons = $resultadoDaBusca[1];
    
        foreach ($listaSons as $som) {
            if($som->getTipoSom() == 2){
                $buscaImagem = ImagemDao::buscaImagemDoSom($som->getIdSom());
                if($buscaImagem[0]){
                    $som->setImagem($buscaImagem[1]);
                }
            }
        }
        $_SESSION['listaSons'] = $listaSons;
        phpRedireciona("../view/realizarTeste.view.php");
    } else {
        $resultadoDaBusca = $resultadoDaBusca[1];
        $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
        phpRedireciona("../view/realizarTeste.view.php?erro=$string");
    }
} else {
    $resultadoDaBusca = $resultadoDaBusca[1];
    $string =  "C1:%20$resultadoDaBusca[0]%20C2:%20$resultadoDaBusca[1]%20M1:%20$resultadoDaBusca[2]";
    phpRedireciona("../view/realizarTeste.view.php?erro=$string");
}