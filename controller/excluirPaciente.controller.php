<?php
session_start();
require_once "../functions/functions.php";
redirecionaUsuarioNaoAutenticado();

$idUsuario = $_SESSION["idUsuario"];
$idPaciente = $_GET["idPaciente"];

require_once "../dao/PacienteDao.class.php";
require_once "../dao/RegistroDao.class.php";
require_once "../dao/ResponsavelDao.class.php";


$resultadoDaExclusaoPaciente = PacienteDao::excluirPacienteUsuario($idPaciente, $idUsuario);
var_dump($resultadoDaExclusaoPaciente);

if($resultadoDaExclusaoPaciente){
    $resultadoRegistro = RegistroDao::excluirRegistrosPaciente($idPaciente);
    if($resultadoRegistro){
        $resultadoResponsavel = ResponsavelDao::excluirResponsavelPaciente($idPaciente);
        if($resultadoResponsavel){
            phpRedireciona("../controller/listaPaciente.controller.php?resultado=8");
        } else {
            $string =  "C1:%20$resultadoResponsavel[0]%20C2:%20$resultadoResponsavel[1]%20M1:%20$resultadoResponsavel[2]";
            phpRedireciona("../view/listaPaciente.view.php?erro=$string");
        }
    } else {
        $string =  "C1:%20$resultadoRegistro[0]%20C2:%20$resultadoRegistro[1]%20M1:%20$resultadoRegistro[2]";
        phpRedireciona("../view/listaPaciente.view.php?erro=$string");
    }
} else {
    $string =  "C1:%20$resultadoDaExclusaoPaciente[0]%20C2:%20$resultadoDaExclusaoPaciente[1]%20M1:%20$resultadoDaExclusaoPaciente[2]";
    phpRedireciona("../view/listaPaciente.view.php?erro=$string");
}